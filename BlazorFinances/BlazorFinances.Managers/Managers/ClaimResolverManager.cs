﻿using BlazorFinances.Managers.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace BlazorFinances.Managers.Managers
{
    public class ClaimResolverManager : IClaimResolverManager
    {
        private readonly IHttpContextAccessor _context;
        public ClaimResolverManager(IHttpContextAccessor context)
        {
            _context = context;
        }

        public ClaimsPrincipal User => _context.HttpContext?.User;
    }
}
