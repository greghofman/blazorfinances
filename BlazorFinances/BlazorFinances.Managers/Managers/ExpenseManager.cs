﻿using AutoMapper;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Accessors.Models;
using BlazorFinances.Engines.Interfaces;
using BlazorFinances.Managers.Interfaces;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace BlazorFinances.Managers.Managers
{
    public class ExpenseManager : IExpenseManager
    {
        private IExpenseAccessor _expenseAccessor;
        private IExpenseEngine _expenseEngine;
        private UserManager<ApplicationUser> _userManager;

        public ExpenseManager(IExpenseAccessor expenseAccessor,
            IExpenseEngine expenseEngine,
            UserManager<ApplicationUser> userManager)
        {
            _expenseAccessor = expenseAccessor;
            _expenseEngine = expenseEngine;
            _userManager = userManager;
        }

        public int Create(ExpenseDto expenseDto)
        {
            var expenseId = _expenseAccessor.Create(expenseDto);

            return expenseId;
        }

        public int Delete(int id)
        {
            _expenseAccessor.Delete(id);

            return id;
        }

        public int Edit(ExpenseDto expenseDto)
        {
            _expenseAccessor.Update(expenseDto);

            return expenseDto.Id;
        }

        public ExpenseDto Get(int id)
        {
            var expenseDto = _expenseAccessor.Get(id);

            return expenseDto;
        }

        public async Task<ExpensesIndexDto> GetAll(string userId, string sortOrder, string searchTerm, int page)
        {
            var user = Mapper.Map<ApplicationUserDto>(await _userManager.FindByIdAsync(userId));
            var expenseDtos = _expenseEngine.GetExpensesForIndex(user, sortOrder, searchTerm, page);

            return expenseDtos;
        }
    }
}
