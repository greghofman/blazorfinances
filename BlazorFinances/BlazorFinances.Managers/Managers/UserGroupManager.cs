﻿using AutoMapper;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Accessors.Models;
using BlazorFinances.Managers.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorFinances.Managers.Managers
{
    public class UserGroupManager : IUserGroupManager
    {
        private IUserGroupAccessor _userGroupAccessor;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserGroupManager(IUserGroupAccessor userGroupAccessor, UserManager<ApplicationUser> userManager)
        {
            _userGroupAccessor = userGroupAccessor;
            _userManager = userManager;
        }

        public UserGroupDto Get(string userId)
        {
            var group = _userManager.Users.Include(x => x.Group).ThenInclude(x => x.Users).Single(x => x.Id == userId).Group;
            var groupDto = Mapper.Map<UserGroupDto>(group);

            return Mapper.Map<UserGroupDto>(groupDto);
        }

        public async Task<int> Create(UserGroupDto userGroupDto, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            user.GroupId = _userGroupAccessor.Create(userGroupDto);
            await _userManager.UpdateAsync(user);

            return user.GroupId.Value;
        }

        public async Task<string> DeleteGroupMember(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            user.GroupId = null;
            await _userManager.UpdateAsync(user);

            return user.Id;
        }

        public async Task<string> Edit(string userId, UserGroupEditDto groupEditDto)
        {
            //this will need to be more involved:
            //do they have an account already? if yes, do they have a group already? if no, then add them to this group. 
            //if no, invite them to the portal and group via email
            //TODO: future: groups should be many to many with users
            var user = await _userManager.FindByIdAsync(userId);
            var groupId = user.GroupId;

            if (!string.IsNullOrEmpty(groupEditDto.NewUserEmail))
            {
                var newGroupUser = await _userManager.FindByNameAsync(groupEditDto.NewUserEmail);
                newGroupUser.GroupId = groupId;
                await _userManager.UpdateAsync(newGroupUser);
            }

            //update group name if changed
            if (!string.IsNullOrEmpty(groupEditDto.NewGroupName))
            {
                _userGroupAccessor.UpdateName(groupId.Value, groupEditDto.NewGroupName);
            }

            return groupEditDto.NewGroupName;
        }
    }
}
