﻿using AutoMapper;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Models;
using BlazorFinances.Engines.Interfaces;
using BlazorFinances.Managers.Interfaces;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace BlazorFinances.Managers.Managers
{
    public class GraphManager : IGraphManager
    {
        private IBarChartEngine _barChartEngine;
        private IPieChartEngine _pieChartEngine;
        private ILineChartEngine _lineChartEngine;
        private IExpenseEngine _expenseEngine;
        private IEarningEngine _earningEngine;
        private UserManager<ApplicationUser> _userManager;

        public GraphManager(IBarChartEngine barChartEngine,
            IPieChartEngine pieChartEngine,
            ILineChartEngine lineChartEngine,
            IExpenseEngine expenseEngine,
            IEarningEngine earningEngine,
            UserManager<ApplicationUser> userManager)
        {
            _barChartEngine = barChartEngine;
            _pieChartEngine = pieChartEngine;
            _lineChartEngine = lineChartEngine;
            _expenseEngine = expenseEngine;
            _earningEngine = earningEngine;
            _userManager = userManager;
        }

        public async Task<BarChartDto> BuildExpensesEarningsBarChart(string userId)
        {
            var user = Mapper.Map<ApplicationUserDto>(await _userManager.FindByIdAsync(userId));
            var groupExpenses = _expenseEngine.GetExpensesForGroup(user);
            var groupEarnings = _earningEngine.GetEarningsForGroup(user);
            var barChartDto = _barChartEngine.BuildExpenseEarningBarChart(groupExpenses, groupEarnings);

            return barChartDto;
        }

        public async Task<PieChartDto> BuildExpensesPieChart(string userId)
        {
            var user = Mapper.Map<ApplicationUserDto>(await _userManager.FindByIdAsync(userId));
            var groupExpenses = _expenseEngine.GetExpensesForGroup(user);
            var barChartDto = _pieChartEngine.BuildExpensesPieChart(groupExpenses);

            return barChartDto;
        }

        public async Task<GoalLineChartDto> BuildExpenseGoalLineChart(string userId, int? goalId)
        {
            var user = Mapper.Map<ApplicationUserDto>(await _userManager.FindByIdAsync(userId));
            var groupExpenses = _expenseEngine.GetExpensesForGroup(user);
            var lineChartDto = _lineChartEngine.BuildExpenseGoalLineChart(groupExpenses, goalId, userId);

            return lineChartDto;
        }
    }
}
