﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Models;
using BlazorFinances.Common.Types;
using BlazorFinances.Engines.Interfaces;
using BlazorFinances.Managers.Interfaces;
using Microsoft.AspNetCore.Identity;
using System.IO;
using System.Threading.Tasks;

namespace BlazorFinances.Managers.Managers
{
    public class ImportManager : IImportManager
    {
        private IBankDataImportEngine _bankImportEngine;

        public ImportManager(IBankDataImportEngine bankImportEngine)
        {
            _bankImportEngine = bankImportEngine;
        }

        public bool ImportCsvBankTransactions(TransactionImportDto import)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(import.Base64FileContents);
            string csvData = System.Text.Encoding.UTF8.GetString(encodedDataAsBytes);

            var success = false;
            switch (import.Bank)
            {
                case BankType.UsBank:
                    success = _bankImportEngine.ImportUsBankTransactionCsvData(csvData, import.UserId);
                    break;
                default:
                    break;
            }

            return success;
        }
    }
}
