﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Common.Types;
using BlazorFinances.Engines.Interfaces;
using BlazorFinances.Managers.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BlazorFinances.Managers.Managers
{
    public class EmailManager : IEmailManager
    {
        private IEmailEngine _emailEngine;

        public EmailManager(IEmailEngine emailEngine)
        {
            _emailEngine = emailEngine;
        }

        public async Task<bool> SendAsync(EmailType emailType, string callbackUrl, ApplicationUserDto user)
        {
            MailMessage email = null;
            switch (emailType)
            {
                case EmailType.ConfirmEmail:
                    email = _emailEngine.GetConfirmEmail(callbackUrl, user);
                    break;
                case EmailType.ForgotPassword:
                    email = _emailEngine.GetForgotPasswordEmail(callbackUrl, user);
                    break;
                default:
                    break;
            }

            var isSuccessful = await _emailEngine.SendEmail(email);

            return isSuccessful;
        }
    }
}
