﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Managers.Interfaces;
using System.Collections.Generic;

namespace BlazorFinances.Managers.Managers
{
    public class CategoryManager : ICategoryManager
    {
        private ICategoryAccessor _categoryAccessor;

        public CategoryManager(ICategoryAccessor categoryAccessor)
        {
            _categoryAccessor = categoryAccessor;
        }

        public int Create(CategoryDto categoryDto)
        {
            var categoryId = _categoryAccessor.Create(categoryDto);

            return categoryId;
        }

        public int Delete(int id)
        {
            _categoryAccessor.Delete(id);

            return id;
        }

        public int Edit(CategoryDto categoryDto)
        {
            _categoryAccessor.Update(categoryDto);

            return categoryDto.Id;
        }

        public CategoryDto Get(int id)
        {
            var categoryDto = _categoryAccessor.Get(id);

            return categoryDto;
        }

        public List<CategoryDto> GetAll()
        {
            var categoryDtos = _categoryAccessor.GetAll();

            return categoryDtos;
        }
    }
}
