﻿using AutoMapper;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Accessors.Models;
using BlazorFinances.Engines.Interfaces;
using BlazorFinances.Managers.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlazorFinances.Managers.Managers
{
    public class EarningManager : IEarningManager
    {
        private IEarningAccessor _earningAccessor;
        private IEarningEngine _earningEngine;
        private readonly UserManager<ApplicationUser> _userManager;

        public EarningManager(IEarningAccessor earningAccessor, 
            IEarningEngine earningEngine,
            UserManager<ApplicationUser> userManager)
        {
            _earningAccessor = earningAccessor;
            _userManager = userManager;
            _earningEngine = earningEngine;
        }

        public int Create(EarningDto earningDto)
        {
            var earningId = _earningAccessor.Create(earningDto);

            return earningId;
        }

        public int Delete(int id)
        {
            _earningAccessor.Delete(id);

            return id;
        }

        public int Edit(EarningDto earningDto)
        {
            _earningAccessor.Update(earningDto);

            return earningDto.Id;
        }

        public EarningDto Get(int id)
        {
            var earningDto = _earningAccessor.Get(id);

            return earningDto;
        }

        public async Task<EarningsIndexDto> GetAll(string userId, string sortOrder, string searchTerm, int page)
        {
            var user = Mapper.Map<ApplicationUserDto>(await _userManager.FindByIdAsync(userId));
            var earningDtos = _earningEngine.GetEarningsForIndex(user, sortOrder, searchTerm, page);

            return earningDtos;
        }
    }
}
