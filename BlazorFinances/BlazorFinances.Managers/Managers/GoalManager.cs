﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Managers.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlazorFinances.Managers.Managers
{
    public class GoalManager : IGoalManager
    {
        private IGoalAccessor _goalAccessor;

        public GoalManager(IGoalAccessor goalAccessor)
        {
            _goalAccessor = goalAccessor;
        }

        public int Create(GoalDto expenseDto)
        {
            var expenseId = _goalAccessor.Create(expenseDto);

            return expenseId;
        }

        public int Delete(int id)
        {
            var deletedId = _goalAccessor.Delete(id);

            return id;
        }

        public int Edit(GoalDto goalDto)
        {
            var editedId = _goalAccessor.Update(goalDto);

            return editedId;
        }

        public GoalDto Get(int id)
        {
            var goalDto = _goalAccessor.Get(id);

            return goalDto;
        }

        public List<GoalDto> GetAll(string userId)
        {
            var goalDtos = _goalAccessor.GetAll(userId);

            return goalDtos;
        }
    }
}
