﻿using BlazorFinances.Accessors.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlazorFinances.Managers.Interfaces
{
    public interface IGoalManager
    {
        List<GoalDto> GetAll(string userId);

        GoalDto Get(int id);

        int Create(GoalDto categoryDto);

        int Edit(GoalDto categoryDto);

        int Delete(int id);
    }
}
