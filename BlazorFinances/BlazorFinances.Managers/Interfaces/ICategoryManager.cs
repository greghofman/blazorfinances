﻿using BlazorFinances.Accessors.DTOs;
using System.Collections.Generic;

namespace BlazorFinances.Managers.Interfaces
{
    public interface ICategoryManager
    {
        List<CategoryDto> GetAll();

        CategoryDto Get(int id);

        int Create(CategoryDto categoryDto);

        int Edit(CategoryDto categoryDto);

        int Delete(int id);
    }
}
