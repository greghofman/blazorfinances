﻿using System.Security.Claims;

namespace BlazorFinances.Managers.Interfaces
{
    public interface IClaimResolverManager
    {
        ClaimsPrincipal User { get; }
    }
}
