﻿using BlazorFinances.Accessors.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlazorFinances.Managers.Interfaces
{
    public interface IExpenseManager
    {
        Task<ExpensesIndexDto> GetAll(string userId, string sortOrder, string searchTerm, int page);

        ExpenseDto Get(int id);

        int Create(ExpenseDto categoryDto);

        int Edit(ExpenseDto categoryDto);

        int Delete(int id);
    }
}
