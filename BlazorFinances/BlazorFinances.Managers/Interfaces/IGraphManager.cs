﻿using BlazorFinances.Accessors.DTOs;
using System.Threading.Tasks;

namespace BlazorFinances.Managers.Interfaces
{
    public interface IGraphManager
    {
        Task<BarChartDto> BuildExpensesEarningsBarChart(string userId);

        Task<PieChartDto> BuildExpensesPieChart(string userId);

        Task<GoalLineChartDto> BuildExpenseGoalLineChart(string userId, int? goalId);
    }
}
