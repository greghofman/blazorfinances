﻿using BlazorFinances.Accessors.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BlazorFinances.Managers.Interfaces
{
    public interface IUserGroupManager
    {
        UserGroupDto Get(string userId);

        Task<int> Create(UserGroupDto userGroupDto, string userId);

        Task<string> Edit(string userId, UserGroupEditDto groupEditDto);

        Task<string> DeleteGroupMember(string email);
    }
}
