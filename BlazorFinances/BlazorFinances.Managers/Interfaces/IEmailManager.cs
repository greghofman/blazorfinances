﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Common.Types;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BlazorFinances.Managers.Interfaces
{
    public interface IEmailManager
    {
        Task<bool> SendAsync(EmailType emailType, string callbackUrl, ApplicationUserDto user);
    }
}
