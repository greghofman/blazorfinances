﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Common.Types;
using System.Threading.Tasks;

namespace BlazorFinances.Managers.Interfaces
{
    public interface IImportManager
    {
        bool ImportCsvBankTransactions(TransactionImportDto import);
    }
}
