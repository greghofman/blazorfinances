﻿namespace BlazorFinances.Managers.Interfaces
{
    public interface IJwtTokenManager
    {
        string BuildJwtToken(string email, string userId);
    }
}
