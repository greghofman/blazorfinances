﻿using BlazorFinances.Accessors.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BlazorFinances.Managers.Interfaces
{
    public interface IEarningManager
    {
        Task<EarningsIndexDto> GetAll(string userId, string sortOrder, string searchTerm, int page);

        EarningDto Get(int id);

        int Create(EarningDto earningDto);

        int Edit(EarningDto earningDto);

        int Delete(int id);
    }
}
