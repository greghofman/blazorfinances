﻿using BlazorFinances.ClientApiShared.ViewModels;
using ChartJs.Blazor.ChartJS.BarChart;
using ChartJs.Blazor.ChartJS.BarChart.Dataset;
using ChartJs.Blazor.ChartJS.Common;
using ChartJs.Blazor.ChartJS.Common.Legends;
using ChartJs.Blazor.ChartJS.LineChart;
using ChartJs.Blazor.ChartJS.PieChart;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlazorFinances.Client.Helpers
{
    public static class ChartJsHelper
    {
        public static LineChartConfig BuildLineChartConfig(GoalLineChartViewModel vm)
        {
            return new LineChartConfig
            {
                CanvasId = "goalLineChart",
                Options = new LineChartOptions
                {
                    Text = "Expenditure Goal Comparison",
                    Display = true,
                    Responsive = true,
                    Legend = new Legend
                    {
                        Position = LegendPosition.RIGHT.ToString(),
                        Labels = new Labels
                        {
                            UsePointStyle = true
                        }
                    },
                    Tooltips = new Tooltips
                    {
                        Mode = Mode.nearest,
                        Intersect = false
                    },
                    Scales = new Scales
                    {
                        xAxes = new List<Axis>
                        {
                            new Axis
                            {
                                Display = "xAxesDisplay",
                                ScaleLabel = new ScaleLabel
                                {
                                    Display = ".",
                                    LabelString = "Months"
                                }
                            }
                        },
                        yAxes = new List<Axis>
                        {
                            new Axis
                            {
                                Display = "yAxes",
                                ScaleLabel = new ScaleLabel
                                {
                                    Display = "..",
                                    LabelString = "$ Amounts"
                                }
                            }
                        }
                    },
                    Hover = new LineChartOptionsHover
                    {
                        Intersect = true,
                        Mode = Mode.nearest
                    }
                },
                Data = new LineChartData
                {
                    Labels = vm.MonthLabels,
                    Datasets = new List<LineChartDataset>
                    {
                        new LineChartDataset
                        {
                            BackgroundColor = "#ff6384",
                            BorderColor = "#ff6384",
                            Label = "Goal Expenses",
                            Data = vm.GoalAmounts,
                            Fill = false,
                            BorderWidth = 2,
                            PointRadius = 3,
                            PointBorderWidth = 1
                        },
                        new LineChartDataset
                        {
                            BackgroundColor = "#007BFF",
                            BorderColor = "#007BFF",
                            Label = "Actual Expenses",
                            Data = vm.MonthlyAmounts,
                            Fill = false,
                            BorderWidth = 2,
                            PointRadius = 3,
                            PointBorderWidth = 1
                        }
                    }
                }
            };
        }

        public static BarChartConfig BuildBarChartConfig(BarChartViewModel vm)
        {
            return new BarChartConfig
            {
                CanvasId = "earningsAndExpenses",
                Options = new BarChartOptions
                {
                    Text = "Earnings and Expenses",
                    Display = true,
                    Responsive = true
                },
                Data = new BarChartData
                {
                    Labels = vm.MonthsLabels,
                    Datasets = new List<BaseBarChartDataset>
                    {
                        new BarChartDataset
                        {
                            BackgroundColor = "#007bff",
                            Label = "Earnings",
                            Data = vm.EarningData,
                            BorderWidth = 1,
                            BorderColor = "#0829FF",
                            HoverBackgroundColor = "#98F095",
                            HoverBorderColor = "#43F049",
                            BorderSkipped = null
                        },
                        new BarChartDataset
                        {
                            BackgroundColor = "#FF0F27",
                            Label = "Expenses",
                            Data = vm.ExpenseData,
                            BorderWidth = 1,
                            HoverBackgroundColor = "#98F095",
                            HoverBorderColor = "#43F049",
                            BorderColor = "#A30A19",
                            BorderSkipped = null,
                        }
                    }
                }
            };
        }

        public static PieChartConfig BuildPieChartConfig(PieChartViewModel vm)
        {
            return new PieChartConfig
            {
                CanvasId = "expensesByCategory",
                Options = new PieChartOptions
                {
                    Text = "Expenses By Category",
                    Display = true,
                    Responsive = true,
                    Animation = new DoughnutAnimation { AnimateRotate = true, AnimateScale = true }
                },
                Data = new PieChartData
                {
                    Labels = vm.PieChartItems.Select(x => x.Label).ToList(),
                    Datasets = new List<PieChartDataset>
                    {
                        new PieChartDataset
                        {
                            BackgroundColor = vm.BackgroundColors.ToArray(),
                            Data = vm.PieChartItems.Select(x => Convert.ToInt32(x.Amount)).ToList(),
                            BorderWidth = 5,
                            HoverBackgroundColor = new [] { "#BEBEBE" },
                            HoverBorderColor = new [] { "#A9A9A9" },
                            HoverBorderWidth = new[] {1},
                            BorderColor = "#ffffff",
                        }
                    }
                }
            };
        }
    }
}
