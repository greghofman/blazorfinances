﻿namespace BlazorFinances.Client
{
    public static class ClientSettings
    {
        public static string GetHostRoot()
        {
#if (DEBUG)
            return @"http://localhost:50466";
#else
            return @"https://blazorfinances.azurewebsites.net";
#endif
        }
    }
}
