﻿using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorFinances.Client
{
    public static class UploadJSInterop
    {
        public static async Task<string> GetFileData(string fileInputRef)
        {
            return (await JSRuntime.Current.InvokeAsync<string>("getFileData", fileInputRef));
        }
    }
}
