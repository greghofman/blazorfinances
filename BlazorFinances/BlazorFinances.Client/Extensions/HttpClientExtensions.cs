﻿using System;
using System.Net.Http;

namespace BlazorFinances.Client.Extensions
{
    public static class HttpClientExtensions
    {
        public static HttpClient GetAuthHttpClient(string token)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", String.Format("Bearer {0}", token));

            return client;
        }
    }
}
