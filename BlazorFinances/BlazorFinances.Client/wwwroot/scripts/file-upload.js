﻿function getBase64File(inputFile) {
    const temporaryFileReader = new FileReader();

    return new Promise((resolve, reject) => {
        temporaryFileReader.onerror = () => {
            temporaryFileReader.abort();
            reject(new DOMException("Problem parsing input file."));
        };
        temporaryFileReader.addEventListener("load", function () {
            console.log("JS : file read done");
            resolve(temporaryFileReader.result.split(',')[1]);
        }, false);
        temporaryFileReader.readAsDataURL(inputFile);
    });
}

getFileData = function (inputFile) {
    var file = document.getElementById(inputFile).files[0];
    var fileContents = getBase64File(file);

    //populate name in custom file upload control
    var fileName = file.name;
    document.getElementById(inputFile + "Label").innerHTML = fileName;

    return fileContents;
};




