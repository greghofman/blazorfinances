﻿using BlazorFinances.Accessors.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorFinances.Accessors.Interfaces
{
    public interface IUserGroupAccessor
    {
        UserGroupDto Get(int id);

        int Create(UserGroupDto userGroupDto);

        void UpdateName(int id, string newName);
    }
}
