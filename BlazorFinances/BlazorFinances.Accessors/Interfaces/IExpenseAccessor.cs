﻿using BlazorFinances.Accessors.DTOs;
using System.Collections.Generic;

namespace BlazorFinances.Accessors.Interfaces
{
    public interface IExpenseAccessor
    {
        ExpenseDto Get(int id);

        List<ExpenseDto> GetAll(List<string> userIds);

        List<ExpenseDto> GetAll(string userId);

        ExpensesIndexDto GetAllForIndex(string userId, string sortOrder, string searchTerm, int page);

        ExpensesIndexDto GetAllForIndex(List<string> userId, string sortOrder, string searchTerm, int page);

        int Create(ExpenseDto expenseDto);

        List<int> CreateAll(List<ExpenseDto> expenseDtos);

        int Update(ExpenseDto expenseDto);

        int Delete(int id);
    }
}
