﻿using BlazorFinances.Accessors.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlazorFinances.Accessors.Interfaces
{
    public interface IGoalAccessor
    {
        int Create(GoalDto goalDto);

        int Delete(int id);

        int Update(GoalDto goalDto);

        GoalDto Get(int id);

        List<GoalDto> GetAll(string userId);
    }
}
