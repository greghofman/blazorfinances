﻿using BlazorFinances.Accessors.DTOs;
using System.Collections.Generic;

namespace BlazorFinances.Accessors.Interfaces
{
    public interface ICategoryAccessor
    {
        CategoryDto Get(int id);

        List<CategoryDto> GetAll();

        int Create(CategoryDto categoryDto);

        int Update(CategoryDto categoryDto);

        int Delete(int id);
    }
}
