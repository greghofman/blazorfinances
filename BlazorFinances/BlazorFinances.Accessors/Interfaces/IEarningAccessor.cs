﻿using BlazorFinances.Accessors.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorFinances.Accessors.Interfaces
{
    public interface IEarningAccessor
    {
        EarningDto Get(int id);

        List<EarningDto> GetAll(string userId);

        List<EarningDto> GetAll(List<string> userId);

        EarningsIndexDto GetAllForIndex(string userId, string sortOrder, string searchTerm, int page);

        EarningsIndexDto GetAllForIndex(List<string> userId, string sortOrder, string searchTerm, int page);

        int Create(EarningDto earningDto);

        List<int> CreateAll(List<EarningDto> earningDtos);

        int Update(EarningDto earningDto);

        int Delete(int id);
    }
}
