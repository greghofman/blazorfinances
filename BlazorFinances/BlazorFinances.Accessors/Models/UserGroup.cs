﻿using BlazorFinances.Accessors.Models.Base;
using System.Collections.Generic;

namespace BlazorFinances.Accessors.Models
{
    public class UserGroup : EntityBase
    {
        public string Name { get; set; }

        public virtual List<ApplicationUser> Users { get; set; }
    }
}
