﻿using Microsoft.AspNetCore.Identity;

namespace BlazorFinances.Accessors.Models
{
    public class ApplicationUser : IdentityUser
    {
        public int? GroupId { get; set; }

        public virtual UserGroup Group { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
