﻿using BlazorFinances.Accessors.Models.Base;
using System;

namespace BlazorFinances.Accessors.Models
{
    public class Earning : EntityBase
  {
    public double Amount { get; set; }

    public string Name { get; set; }

    public DateTime OccurenceDate { get; set; }

    public int? CategoryId { get; set; }

    public virtual Category Category { get; set; }

    public string UserId { get; set; }

    public virtual ApplicationUser User { get; set; }
  }
}
