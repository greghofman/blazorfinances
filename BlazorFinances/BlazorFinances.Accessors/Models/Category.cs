﻿using BlazorFinances.Accessors.Models.Base;

namespace BlazorFinances.Accessors.Models
{
    public class Category : EntityBase
  {
    public string Name { get; set; }
  }
}
