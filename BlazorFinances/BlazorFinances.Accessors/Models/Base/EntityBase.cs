﻿using System.ComponentModel.DataAnnotations;

namespace BlazorFinances.Accessors.Models.Base
{
    public class EntityBase
  {
    [Key]
    public int Id { get; set; }
  }
}
