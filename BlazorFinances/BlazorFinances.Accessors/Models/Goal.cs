﻿using BlazorFinances.Accessors.Models.Base;

namespace BlazorFinances.Accessors.Models
{
    public class Goal : EntityBase
    {
        public string Name { get; set; }

        public int? CategoryId { get; set; }

        public Category Category { get; set; }

        public decimal Amount { get; set; }

        public bool IsForTotalExpenses { get; set; }

        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}
