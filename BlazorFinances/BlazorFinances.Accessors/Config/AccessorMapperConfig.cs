﻿using AutoMapper;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Models;

namespace BlazorFinances.Accessors.Config
{
    public class AccessorMapperConfig : Profile
  {
    public AccessorMapperConfig()
    {
      CreateMap<Category, CategoryDto>();
      CreateMap<CategoryDto, Category>();

      CreateMap<Earning, EarningDto>();
      CreateMap<EarningDto, Earning>();

      CreateMap<Expense, ExpenseDto>();
      CreateMap<ExpenseDto, Expense>();

      CreateMap<UserGroup, UserGroupDto>();
      CreateMap<UserGroupDto, UserGroup>();
    }

    public override string ProfileName
    {
      get { return this.GetType().ToString(); }
    }
  }
}
