﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace BlazorFinances.Accessors
{
  public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<FinancesContext>
  {
    public FinancesContext CreateDbContext(string[] args)
    {
      var builder = new DbContextOptionsBuilder<FinancesContext>();
      builder.UseSqlServer("Data Source=.\\SQLEXPRESS;Initial Catalog=FinancesDb;Integrated Security=True;");

      return new FinancesContext(builder.Options);
    }
  }
}
