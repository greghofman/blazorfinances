﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BlazorFinances.Accessors.Migrations
{
    public partial class GoalAmount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Amount",
                table: "Goals",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Amount",
                table: "Goals");
        }
    }
}
