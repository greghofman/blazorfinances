﻿using AutoMapper;
using BlazorFinances.Accessors.Accessors.Base;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Accessors.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlazorFinances.Accessors.Accessors
{
    public class ExpenseAccessor : AccessorBase, IExpenseAccessor
    {
        public ExpenseAccessor(IConfiguration config) : base(config) { }

        private const int _pageSize = 10;

        public int Create(ExpenseDto expenseDto)
        {
            var expense = Mapper.Map<Expense>(expenseDto);

            using (var context = new FinancesContext(GetDbOptions()))
            {
                context.Expenses.Add(expense);
                context.SaveChanges();
            }

            return expense.Id;
        }

        public List<int> CreateAll(List<ExpenseDto> expenseDtos)
        {
            var expenses = Mapper.Map<List<Expense>>(expenseDtos);

            using (var context = new FinancesContext(GetDbOptions()))
            {
                context.Expenses.AddRange(expenses);
                context.SaveChanges();
            }

            return expenses.Select(x => x.Id).ToList();
        }

        public int Delete(int id)
        {
            using (var context = new FinancesContext(GetDbOptions()))
            {
                var expense = context.Expenses.Single(x => x.Id == id);
                context.Expenses.Remove(expense);
                context.SaveChanges();
            }

            return id;
        }

        public ExpenseDto Get(int id)
        {
            Expense expense = null;

            using (var context = new FinancesContext(GetDbOptions()))
            {
                expense = context.Expenses.Single(x => x.Id == id);
            }

            return Mapper.Map<ExpenseDto>(expense);
        }

        public List<ExpenseDto> GetAll(string userId)
        {
            List<Expense> expenses = null;

            using (var context = new FinancesContext(GetDbOptions()))
            {
                expenses = context.Expenses
                    .Include(x => x.Category)
                    .Include(x => x.User)
                    .Where(x => x.UserId == userId)
                    .ToList();
            }

            return Mapper.Map<List<ExpenseDto>>(expenses);
        }

        public List<ExpenseDto> GetAll(List<string> userIds)
        {
            List<Expense> expenses = null;

            using (var context = new FinancesContext(GetDbOptions()))
            {
                expenses = context.Expenses
                    .Include(x => x.Category)
                    .Include(x => x.User)
                    .Where(x => userIds.Contains(x.UserId))
                    .ToList();
            }

            return Mapper.Map<List<ExpenseDto>>(expenses);
        }

        public int Update(ExpenseDto expenseDto)
        {
            var expense = Mapper.Map<Expense>(expenseDto);

            using (var context = new FinancesContext(GetDbOptions()))
            {
                context.Entry(expense).State = EntityState.Modified;
                context.SaveChanges();
            }

            return expenseDto.Id;
        }

        public ExpensesIndexDto GetAllForIndex(string userId, string sortOrder, string searchTerm, int page)
        {
            List<Expense> expenses = null;
            var totalRows = 0;
            var totalFilteredRows = 0;
            using (var context = new FinancesContext(GetDbOptions()))
            {
                var query = context.Expenses
                    .Include(x => x.Category)
                    .Include(x => x.User)
                    .Where(x => x.UserId == userId);

                totalRows = query.Count();

                switch (sortOrder)
                {
                    case "user":
                        query = query.OrderBy(x => x.User.UserName);
                        break;
                    case "user_desc":
                        query = query.OrderByDescending(x => x.User.UserName);
                        break;
                    case "name":
                        query = query.OrderBy(x => x.Name);
                        break;
                    case "name_desc":
                        query = query.OrderByDescending(x => x.Name);
                        break;
                    case "amount":
                        query = query.OrderBy(x => x.Amount);
                        break;
                    case "amount_desc":
                        query = query.OrderByDescending(x => x.Amount);
                        break;
                    case "date":
                        query = query.OrderBy(x => x.OccurenceDate);
                        break;
                    case "date_desc":
                        query = query.OrderByDescending(x => x.OccurenceDate);
                        break;
                    case "category":
                        query = query.OrderBy(x => x.Category.Name);
                        break;
                    case "category_desc":
                        query = query.OrderByDescending(x => x.Category.Name);
                        break;
                    default:
                        query = query.OrderBy(x => x.User.UserName);
                        break;
                }

                if (!string.IsNullOrEmpty(searchTerm))
                {
                    query = query.Where(x => x.User.UserName.Contains(searchTerm) ||
                    x.Name.Contains(searchTerm) ||
                    x.Amount.ToString().Contains(searchTerm) ||
                    x.OccurenceDate.ToString().Contains(searchTerm) ||
                    x.Category.Name.Contains(searchTerm));
                }

                totalFilteredRows = query.Count();

                if ((page * 10) - 9 > totalFilteredRows)
                {
                    var pageNonRounded = (double)((decimal)totalFilteredRows / (decimal)10);
                    var newPage = (int)Math.Floor(pageNonRounded);
                    var vm = GetAllForIndex(userId, sortOrder, searchTerm, newPage);
                    return vm;
                }

                query = query.Skip(page * _pageSize).Take(_pageSize);

                expenses = query.ToList();
            }

            double pageCount = (double)((decimal)totalRows / Convert.ToDecimal(_pageSize));

            return new ExpensesIndexDto
            {
                Expenses = Mapper.Map<List<ExpenseDto>>(expenses),
                Page = page,
                SearchTerm = searchTerm,
                SortOrder = sortOrder,
                TotalPages = (int)Math.Ceiling(pageCount),
                TotalRecords = totalRows,
                TotalFilteredRecords = totalFilteredRows
            };
        }

        public ExpensesIndexDto GetAllForIndex(List<string> userIds, string sortOrder, string searchTerm, int page)
        {
            List<Expense> expenses = null;
            var totalRows = 0;
            var totalFilteredRows = 0;

            using (var context = new FinancesContext(GetDbOptions()))
            {
                var query = context.Expenses
                    .Include(x => x.Category)
                    .Include(x => x.User)
                    .Where(x => userIds.Contains(x.UserId));

                totalRows = query.Count();

                switch (sortOrder)
                {
                    case "user":
                        query = query.OrderBy(x => x.User.UserName);
                        break;
                    case "user_desc":
                        query = query.OrderByDescending(x => x.User.UserName);
                        break;
                    case "name":
                        query = query.OrderBy(x => x.Name);
                        break;
                    case "name_desc":
                        query = query.OrderByDescending(x => x.Name);
                        break;
                    case "amount":
                        query = query.OrderBy(x => x.Amount);
                        break;
                    case "amount_desc":
                        query = query.OrderByDescending(x => x.Amount);
                        break;
                    case "date":
                        query = query.OrderBy(x => x.OccurenceDate);
                        break;
                    case "date_desc":
                        query = query.OrderByDescending(x => x.OccurenceDate);
                        break;
                    case "category":
                        query = query.OrderBy(x => x.Category.Name);
                        break;
                    case "category_desc":
                        query = query.OrderByDescending(x => x.Category.Name);
                        break;
                    default:
                        query = query.OrderBy(x => x.User.UserName);
                        break;
                }

                if (!string.IsNullOrEmpty(searchTerm))
                {
                    query = query.Where(x => x.User.UserName.Contains(searchTerm) ||
                    x.Name.Contains(searchTerm) ||
                    x.Amount.ToString().Contains(searchTerm) ||
                    x.OccurenceDate.ToString().Contains(searchTerm) ||
                    x.Category.Name.Contains(searchTerm));
                }

                totalFilteredRows = query.Count();

                if ((page * 10) - 9 > totalFilteredRows)
                {
                    var pageNonRounded = (double)((decimal)totalFilteredRows / (decimal)10);
                    var newPage = (int)Math.Floor(pageNonRounded);
                    var vm = GetAllForIndex(userIds, sortOrder, searchTerm, newPage);
                    return vm;
                }

                query = query.Skip(page * _pageSize).Take(_pageSize);

                expenses = query.ToList();
            }

            double pageCount = (double)((decimal)totalRows / Convert.ToDecimal(_pageSize));

            return new ExpensesIndexDto
            {
                Expenses = Mapper.Map<List<ExpenseDto>>(expenses),
                Page = page,
                SearchTerm = searchTerm,
                SortOrder = sortOrder,
                TotalPages = (int)Math.Ceiling(pageCount),
                TotalRecords = totalRows
            };
        }
    }
}
