﻿using AutoMapper;
using BlazorFinances.Accessors.Accessors.Base;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Accessors.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlazorFinances.Accessors.Accessors
{
    public class UserGroupAccessor : AccessorBase, IUserGroupAccessor
    {
        public UserGroupAccessor(IConfiguration config) : base(config) { }

        public int Create(UserGroupDto userGroupDto)
        {
            var userGroup = Mapper.Map<UserGroup>(userGroupDto);

            using (var context = new FinancesContext(GetDbOptions()))
            {
                context.UserGroups.Add(userGroup);
                context.SaveChanges();
            }

            return userGroup.Id;
        }

        public UserGroupDto Get(int id)
        {
            UserGroup userGroup = null;

            using (var context = new FinancesContext(GetDbOptions()))
            {
                userGroup = context.UserGroups.Include(x => x.Users).Single(x => x.Id == id);
            }

            return Mapper.Map<UserGroupDto>(userGroup);
        }

        public void UpdateName(int id, string newGroupName)
        {
            using (var context = new FinancesContext(GetDbOptions()))
            {
                var group = context.UserGroups.Single(x => x.Id == id);
                group.Name = newGroupName;
                context.SaveChanges();
            }

            return;
        }
    }
}
