﻿using AutoMapper;
using BlazorFinances.Accessors.Accessors.Base;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Accessors.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlazorFinances.Accessors.Accessors
{
    public class EarningAccessor : AccessorBase, IEarningAccessor
    {
        public EarningAccessor(IConfiguration config) : base(config) { }

        private const int _pageSize = 10;

        public int Create(EarningDto earningDto)
        {
            var earning = Mapper.Map<Earning>(earningDto);

            using (var context = new FinancesContext(GetDbOptions()))
            {
                context.Earnings.Add(earning);
                context.SaveChanges();
            }

            return earning.Id;
        }

        public List<int> CreateAll(List<EarningDto> earningDtos)
        {
            var earnings = Mapper.Map<List<Earning>>(earningDtos);

            using (var context = new FinancesContext(GetDbOptions()))
            {
                context.Earnings.AddRange(earnings);
                context.SaveChanges();
            }

            return earnings.Select(x => x.Id).ToList();
        }

        public int Delete(int id)
        {
            using (var context = new FinancesContext(GetDbOptions()))
            {
                var earning = context.Earnings.Single(x => x.Id == id);
                context.Earnings.Remove(earning);
                context.SaveChanges();
            }

            return id;
        }

        public EarningDto Get(int id)
        {
            Earning earning = null;

            using (var context = new FinancesContext(GetDbOptions()))
            {
                earning = context.Earnings.Single(x => x.Id == id);
            }

            return Mapper.Map<EarningDto>(earning);
        }

        public List<EarningDto> GetAll(string userId)
        {
            List<Earning> earnings = null;

            using (var context = new FinancesContext(GetDbOptions()))
            {
                earnings = context.Earnings
                    .Include(x => x.Category)
                    .Include(x => x.User)
                    .Where(x => x.UserId == userId)
                    .ToList();
            }

            return Mapper.Map<List<EarningDto>>(earnings);
        }

        public List<EarningDto> GetAll(List<string> userIds)
        {
            List<Earning> earnings = null;

            using (var context = new FinancesContext(GetDbOptions()))
            {
                earnings = context.Earnings
                    .Include(x => x.Category)
                    .Include(x => x.User)
                    .Where(x => userIds.Contains(x.UserId))
                    .ToList();
            }

            return Mapper.Map<List<EarningDto>>(earnings);
        }

        public EarningsIndexDto GetAllForIndex(string userId, string sortOrder, string searchTerm, int page)
        {
            List<Earning> earnings = null;
            var totalRows = 0;
            var totalFilteredRows = 0;
            using (var context = new FinancesContext(GetDbOptions()))
            {
                var query = context.Earnings
                    .Include(x => x.Category)
                    .Include(x => x.User)
                    .Where(x => x.UserId == userId);

                totalRows = query.Count();

                switch (sortOrder)
                {
                    case "user":
                        query = query.OrderBy(x => x.User.UserName);
                        break;
                    case "user_desc":
                        query = query.OrderByDescending(x => x.User.UserName);
                        break;
                    case "name":
                        query = query.OrderBy(x => x.Name);
                        break;
                    case "name_desc":
                        query = query.OrderByDescending(x => x.Name);
                        break;
                    case "amount":
                        query = query.OrderBy(x => x.Amount);
                        break;
                    case "amount_desc":
                        query = query.OrderByDescending(x => x.Amount);
                        break;
                    case "date":
                        query = query.OrderBy(x => x.OccurenceDate);
                        break;
                    case "date_desc":
                        query = query.OrderByDescending(x => x.OccurenceDate);
                        break;
                    case "category":
                        query = query.OrderBy(x => x.Category.Name);
                        break;
                    case "category_desc":
                        query = query.OrderByDescending(x => x.Category.Name);
                        break;
                    default:
                        query = query.OrderBy(x => x.User.UserName);
                        break;
                }

                if (!string.IsNullOrEmpty(searchTerm))
                {
                    query = query.Where(x => x.User.UserName.Contains(searchTerm) ||
                    x.Name.Contains(searchTerm) ||
                    x.Amount.ToString().Contains(searchTerm) ||
                    x.OccurenceDate.ToString().Contains(searchTerm) ||
                    x.Category.Name.Contains(searchTerm));
                }

                totalFilteredRows = query.Count();

                if ((page * 10) - 9 > totalFilteredRows)
                {
                    var pageNonRounded = (double)((decimal)totalFilteredRows / (decimal)10);
                    var newPage = (int)Math.Floor(pageNonRounded);
                    var vm = GetAllForIndex(userId, sortOrder, searchTerm, newPage);
                    return vm;
                }

                query = query.Skip(page * _pageSize).Take(_pageSize);

                earnings = query.ToList();
            }

            double pageCount = (double)((decimal)totalRows / Convert.ToDecimal(_pageSize));

            return new EarningsIndexDto
            {
                Earnings = Mapper.Map<List<EarningDto>>(earnings),
                Page = page,
                SearchTerm = searchTerm,
                SortOrder = sortOrder,
                TotalPages = (int)Math.Ceiling(pageCount),
                TotalRecords = totalRows,
                TotalFilteredRecords = totalFilteredRows
            };
        }

        public EarningsIndexDto GetAllForIndex(List<string> userIds, string sortOrder, string searchTerm, int page)
        {
            List<Earning> earnings = null;
            var totalRows = 0;
            var totalFilteredRows = 0;

            using (var context = new FinancesContext(GetDbOptions()))
            {
                var query = context.Earnings
                    .Include(x => x.Category)
                    .Include(x => x.User)
                    .Where(x => userIds.Contains(x.UserId));

                totalRows = query.Count();

                switch (sortOrder)
                {
                    case "user":
                        query = query.OrderBy(x => x.User.UserName);
                        break;
                    case "user_desc":
                        query = query.OrderByDescending(x => x.User.UserName);
                        break;
                    case "name":
                        query = query.OrderBy(x => x.Name);
                        break;
                    case "name_desc":
                        query = query.OrderByDescending(x => x.Name);
                        break;
                    case "amount":
                        query = query.OrderBy(x => x.Amount);
                        break;
                    case "amount_desc":
                        query = query.OrderByDescending(x => x.Amount);
                        break;
                    case "date":
                        query = query.OrderBy(x => x.OccurenceDate);
                        break;
                    case "date_desc":
                        query = query.OrderByDescending(x => x.OccurenceDate);
                        break;
                    case "category":
                        query = query.OrderBy(x => x.Category.Name);
                        break;
                    case "category_desc":
                        query = query.OrderByDescending(x => x.Category.Name);
                        break;
                    default:
                        query = query.OrderBy(x => x.User.UserName);
                        break;
                }

                if (!string.IsNullOrEmpty(searchTerm))
                {
                    query = query.Where(x => x.User.UserName.Contains(searchTerm) ||
                    x.Name.Contains(searchTerm) ||
                    x.Amount.ToString().Contains(searchTerm) ||
                    x.OccurenceDate.ToString().Contains(searchTerm) ||
                    x.Category.Name.Contains(searchTerm));
                }

                totalFilteredRows = query.Count();

                if ((page * 10) - 9 > totalFilteredRows)
                {
                    var pageNonRounded = (double)((decimal) totalFilteredRows / (decimal) 10);
                    var newPage = (int)Math.Floor(pageNonRounded);
                    var vm  = GetAllForIndex(userIds, sortOrder, searchTerm, newPage);
                    return vm;
                }

                query = query.Skip(page * _pageSize).Take(_pageSize);

                earnings = query.ToList();
            }

            double pageCount = (double)((decimal)totalRows / Convert.ToDecimal(_pageSize));

            return new EarningsIndexDto
            {
                Earnings = Mapper.Map<List<EarningDto>>(earnings),
                Page = page,
                SearchTerm = searchTerm,
                SortOrder = sortOrder,
                TotalPages = (int)Math.Ceiling(pageCount),
                TotalRecords = totalRows
            };
        }

        public int Update(EarningDto earningDto)
        {
            var earning = Mapper.Map<Earning>(earningDto);

            using (var context = new FinancesContext(GetDbOptions()))
            {
                context.Entry(earning).State = EntityState.Modified;
                context.SaveChanges();
            }

            return earning.Id;
        }
    }
}
