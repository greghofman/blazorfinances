﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BlazorFinances.Accessors.Accessors.Base;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Accessors.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BlazorFinances.Accessors.Accessors
{
    public class GoalAccessor : AccessorBase, IGoalAccessor
    {
        public GoalAccessor(IConfiguration config) : base(config) { }

        public int Create(GoalDto goalDto)
        {
            var goal = Mapper.Map<Goal>(goalDto);

            using (var context = new FinancesContext(GetDbOptions()))
            {
                context.Goals.Add(goal);
                context.SaveChanges();
            }

            return goal.Id;
        }

        public int Delete(int id)
        {
            using (var context = new FinancesContext(GetDbOptions()))
            {
                var earning = context.Goals.Single(x => x.Id == id);
                context.Goals.Remove(earning);
                context.SaveChanges();
            }

            return id;
        }

        public GoalDto Get(int id)
        {
            Goal goal = null;

            using (var context = new FinancesContext(GetDbOptions()))
            {
                goal = context.Goals.Include(x => x.Category).Single(x => x.Id == id);
            }

            return Mapper.Map<GoalDto>(goal);
        }

        public List<GoalDto> GetAll(string userId)
        {
            List<Goal> goals = null;

            using (var context = new FinancesContext(GetDbOptions()))
            {
                goals = context.Goals.Include(x => x.Category).Where(x => x.UserId == userId).ToList();
            }

            return Mapper.Map<List<GoalDto>>(goals);
        }

        public int Update(GoalDto goalDto)
        {
            var goal = Mapper.Map<Goal>(goalDto);

            using (var context = new FinancesContext(GetDbOptions()))
            {
                context.Entry(goal).State = EntityState.Modified;
                context.SaveChanges();
            }

            return goal.Id;
        }
    }
}
