﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BlazorFinances.Accessors.Accessors.Base
{
    public class AccessorBase
    {
        private IConfiguration _config;

        public AccessorBase(IConfiguration config)
        {
            _config = config;
        }

        protected DbContextOptions<FinancesContext> GetDbOptions()
        {
            DbContextOptionsBuilder<FinancesContext> optionsBuilder = new DbContextOptionsBuilder<FinancesContext>();
            optionsBuilder.UseSqlServer(_config.GetConnectionString("BlazorFinances.Accessors.FinancesContext"));

            return optionsBuilder.Options;
        }
    }
}
