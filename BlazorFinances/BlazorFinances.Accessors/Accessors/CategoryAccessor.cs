﻿using AutoMapper;
using BlazorFinances.Accessors.Accessors.Base;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Accessors.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace BlazorFinances.Accessors.Accessors
{
    public class CategoryAccessor : AccessorBase, ICategoryAccessor
    {
        public CategoryAccessor(IConfiguration config) : base(config) { }

        public int Create(CategoryDto categoryDto)
        {
            var category = Mapper.Map<Category>(categoryDto);

            using (var context = new FinancesContext(GetDbOptions()))
            {
                context.Categories.Add(category);
                context.SaveChanges();
            }

            return category.Id;
        }

        public int Delete(int id)
        {
            using (var context = new FinancesContext(GetDbOptions()))
            {
                var category = context.Categories.Single(x => x.Id == id);
                context.Categories.Remove(category);
                context.SaveChanges();
            }

            return id;
        }

        public CategoryDto Get(int id)
        {
            Category category = null;

            using (var context = new FinancesContext(GetDbOptions()))
            {
                category = context.Categories.Single(x => x.Id == id);
            }

            return Mapper.Map<CategoryDto>(category);
        }

        public List<CategoryDto> GetAll()
        {
            List<Category> categories = null;

            using (var context = new FinancesContext(GetDbOptions()))
            {
                categories = context.Categories.ToList();
            }

            return Mapper.Map<List<CategoryDto>>(categories);
        }

        public int Update(CategoryDto categoryDto)
        {
            var category = Mapper.Map<Category>(categoryDto);

            using (var context = new FinancesContext(GetDbOptions()))
            {
                context.Entry(category).State = EntityState.Modified;
                context.SaveChanges();
            }

            return categoryDto.Id;
        }
    }
}
