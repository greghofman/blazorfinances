﻿using BlazorFinances.Accessors.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BlazorFinances.Accessors
{
    public class FinancesContext : IdentityDbContext
    {
        public FinancesContext(DbContextOptions<FinancesContext> options)
            : base(options)
        {
        }

        public DbSet<Expense> Expenses { get; set; }

        public DbSet<Earning> Earnings { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<UserGroup> UserGroups { get; set; }

        public DbSet<Goal> Goals { get; set; }
    }
}
