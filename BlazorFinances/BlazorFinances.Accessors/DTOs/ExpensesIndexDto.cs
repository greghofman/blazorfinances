﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorFinances.Accessors.DTOs
{
    public class ExpensesIndexDto
    {
        public string SortOrder { get; set; }

        public string SearchTerm { get; set; }

        public int Page { get; set; }

        public int TotalPages { get; set; }

        public int TotalRecords { get; set; }

        public int TotalFilteredRecords { get; set; }

        public List<ExpenseDto> Expenses { get; set; }
    }
}
