﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorFinances.Accessors.DTOs
{
    public class UserGroupEditDto
    {
        public string NewUserEmail { get; set; }

        public string NewGroupName { get; set; }
    }
}
