﻿namespace BlazorFinances.Accessors.DTOs
{
    public class PieChartItemDto
    {
        public string Label { get; set; }

        public double Amount { get; set; }
    }
}
