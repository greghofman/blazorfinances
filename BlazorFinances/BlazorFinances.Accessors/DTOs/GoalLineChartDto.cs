﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorFinances.Accessors.DTOs
{
    public class GoalLineChartDto
    {
        public List<object> MonthlyAmounts { get; set; }

        public List<object> GoalAmounts { get; set; }

        public List<string> MonthLabels { get; set; }
    }
}
