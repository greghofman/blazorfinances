﻿using BlazorFinances.Accessors.DTOs.Base;
using System.Collections.Generic;

namespace BlazorFinances.Accessors.DTOs
{
    public class UserGroupDto : DtoBase
    {
        public string Name { get; set; }

        public List<ApplicationUserDto> Users { get; set; }
    }
}
