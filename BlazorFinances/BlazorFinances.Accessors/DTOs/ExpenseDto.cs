﻿using BlazorFinances.Accessors.DTOs.Base;
using BlazorFinances.Common.Helpers;
using System;

namespace BlazorFinances.Accessors.DTOs
{
    public class ExpenseDto : DtoBase
    {
        public double Amount { get; set; }

        public string TextualAmount {
            get {
                return Amount != 0d ? NumberToWordHelper.NumberToWords(Amount) : string.Empty;
            }
        }
        public string Name { get; set; }

        public DateTime OccurenceDate { get; set; }

        public int? CategoryId { get; set; }

        public virtual CategoryDto Category { get; set; }

        public string UserId { get; set; }

        public virtual ApplicationUserDto User { get; set; }
    }
}
