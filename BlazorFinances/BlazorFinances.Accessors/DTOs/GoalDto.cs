﻿using BlazorFinances.Accessors.DTOs.Base;

namespace BlazorFinances.Accessors.DTOs
{
    public class GoalDto : DtoBase
    {
        public string Name { get; set; }

        public int? CategoryId { get; set; }

        public CategoryDto Category { get; set; }

        public decimal Amount { get; set; }

        public bool IsForTotalExpenses { get; set; }

        public string UserId { get; set; }

        public virtual ApplicationUserDto User { get; set; }
    }
}
