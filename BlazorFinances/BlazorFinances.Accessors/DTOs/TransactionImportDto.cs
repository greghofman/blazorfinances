﻿using BlazorFinances.Common.Types;
using Microsoft.AspNetCore.Http;

namespace BlazorFinances.Accessors.DTOs
{
    public class TransactionImportDto
    {
        public string Base64FileContents { get; set; }

        public BankType Bank { get; set; }

        public string UserId { get; set; }
    }
}
