﻿using BlazorFinances.Accessors.DTOs.Base;

namespace BlazorFinances.Accessors.DTOs
{
    public class CategoryDto : DtoBase
  {
    public string Name { get; set; }
  }
}
