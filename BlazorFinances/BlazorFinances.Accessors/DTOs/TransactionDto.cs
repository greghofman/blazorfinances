﻿using System;

namespace BlazorFinances.Accessors.DTOs
{
    public class TransactionDto
    {
        public string Name { get; set; }

        public double Amount { get; set; }

        public DateTime OccurenceDate { get; set; }
    }
}
