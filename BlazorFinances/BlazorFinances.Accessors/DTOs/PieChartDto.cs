﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorFinances.Accessors.DTOs
{
    public class PieChartDto
    {
        public List<PieChartItemDto> PieChartItems { get; set; }

        public List<string> BackgroundColors { get; set; }
    }
}
