﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorFinances.Accessors.DTOs
{
    public class BarChartDto
    {
        public List<object> ExpenseData { get; set; }

        public List<object> EarningData { get; set; }

        public List<string> MonthsLabels { get; set; }
    }
}
