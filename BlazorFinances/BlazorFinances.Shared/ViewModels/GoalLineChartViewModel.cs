﻿using System.Collections.Generic;

namespace BlazorFinances.ClientApiShared.ViewModels
{
    public class GoalLineChartViewModel
    {
        public List<object> MonthlyAmounts { get; set; }

        public List<object> GoalAmounts { get; set; }

        public List<string> MonthLabels { get; set; }
    }
}
