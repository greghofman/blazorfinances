﻿using BlazorFinances.ClientApiShared.ViewModels.Account;
using BlazorFinances.ClientApiShared.ViewModels.Base;

namespace BlazorFinances.ClientApiShared.ViewModels
{
    public class GoalViewModel : ViewModelBase
    {
        public string Name { get; set; }

        public int? CategoryId { get; set; }

        public CategoryViewModel Category { get; set; }

        public decimal Amount { get; set; }

        public bool IsForTotalExpenses { get; set; }

        public string UserId { get; set; }

        public virtual ApplicationUserViewModel User { get; set; }
    }
}
