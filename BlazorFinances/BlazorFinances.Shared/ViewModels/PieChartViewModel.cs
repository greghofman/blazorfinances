﻿using System.Collections.Generic;

namespace BlazorFinances.ClientApiShared.ViewModels
{
    public class PieChartViewModel
    {
        public List<PieChartItemViewModel> PieChartItems { get; set; }

        public List<string> BackgroundColors { get; set; }
    }
}
