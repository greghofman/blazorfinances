﻿namespace BlazorFinances.ClientApiShared.ViewModels
{
    public class PieChartItemViewModel
    {
        public string Label { get; set; }

        public double Amount { get; set; }
    }
}
