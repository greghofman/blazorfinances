﻿using BlazorFinances.ClientApiShared.ViewModels.Base;

namespace BlazorFinances.ClientApiShared.ViewModels
{
    public class CategoryViewModel : ViewModelBase
  {
    public string Name { get; set; }
  }
}
