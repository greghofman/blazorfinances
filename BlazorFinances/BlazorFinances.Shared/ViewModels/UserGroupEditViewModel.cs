﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorFinances.ClientApiShared.ViewModels
{
    public class UserGroupEditViewModel
    {
        public string NewUserEmail { get; set; }

        public string NewGroupName { get; set; }
    }
}
