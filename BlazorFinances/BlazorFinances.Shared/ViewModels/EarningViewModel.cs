﻿using BlazorFinances.ClientApiShared.ViewModels.Account;
using BlazorFinances.ClientApiShared.ViewModels.Base;
using System;

namespace BlazorFinances.ClientApiShared.ViewModels
{
    public class EarningViewModel : ViewModelBase
    {
        public double Amount { get; set; }

        public string TextualAmount { get; set; }

        public string Name { get; set; }

        public DateTime OccurenceDate { get; set; }

        public int? CategoryId { get; set; }

        public virtual CategoryViewModel Category { get; set; }

        public string UserId { get; set; }

        public virtual ApplicationUserViewModel User { get; set; }
    }
}
