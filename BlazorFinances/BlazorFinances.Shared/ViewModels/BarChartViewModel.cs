﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorFinances.ClientApiShared.ViewModels
{
    public class BarChartViewModel
    {
        public List<object> ExpenseData { get; set; }

        public List<object> EarningData { get; set; }

        public List<string> MonthsLabels { get; set; }
    }
}
