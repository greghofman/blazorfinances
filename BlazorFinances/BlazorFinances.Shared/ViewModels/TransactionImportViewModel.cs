﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using BlazorFinances.Common.Types;

namespace BlazorFinances.ClientApiShared.ViewModels
{
    public class TransactionImportViewModel
    {
        public string Base64FileContents { get; set; }

        public BankType Bank { get; set; }

        public string UserId { get; set; }
    }
}
