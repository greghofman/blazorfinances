﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorFinances.ClientApiShared.ViewModels.Account
{
    public class ApplicationUserViewModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int? GroupId { get; set; }
    }
}
