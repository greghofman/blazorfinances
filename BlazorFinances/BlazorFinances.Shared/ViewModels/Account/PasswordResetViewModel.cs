﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorFinances.ClientApiShared.ViewModels.Account
{
    public class PasswordResetViewModel
    {
        public string Token { get; set; }

        public string UserId { get; set; }

        public string Password { get; set; }
    }
}
