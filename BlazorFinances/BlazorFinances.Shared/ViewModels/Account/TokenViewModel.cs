﻿namespace BlazorFinances.ClientApiShared.ViewModels.Account
{
    //TODO: rename to something more sensical, loginviewmodel? and add validation and confirmemail, rememberme, etc
    public class TokenViewModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string Token { get; set; }

        public bool IsSuccessful { get; set; }

        public bool IsEmailConfirmed { get; set; }
    }
}
