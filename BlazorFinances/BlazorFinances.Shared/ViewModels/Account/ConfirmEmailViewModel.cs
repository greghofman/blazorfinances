﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorFinances.ClientApiShared.ViewModels.Account
{
    public class ConfirmEmailViewModel
    {
        public string Token { get; set; }

        public string UserId { get; set; }
    }
}
