﻿using BlazorFinances.ClientApiShared.ViewModels.Account;
using BlazorFinances.ClientApiShared.ViewModels.Base;
using System.Collections.Generic;

namespace BlazorFinances.ClientApiShared.ViewModels
{
    public class UserGroupViewModel : ViewModelBase
    {
        public string Name { get; set; }

        public List<ApplicationUserViewModel> Users { get; set; }
    }
}
