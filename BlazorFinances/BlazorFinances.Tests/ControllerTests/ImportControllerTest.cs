﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Api.Controllers;
using BlazorFinances.ClientApiShared.ViewModels;
using BlazorFinances.Managers.Interfaces;
using BlazorFinances.Tests.ControllerTests.Base;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace BlazorFinances.Tests.ControllerTests
{
    public class ImportControllerTest : BaseControllerTest
    {
        private Mock<IImportManager> _importManagerMock;
        private ImportController _importController;

        public ImportControllerTest()
        {
            _importManagerMock = new Mock<IImportManager>();
            _importController = new ImportController(_importManagerMock.Object, _mockClaimResolverManager.Object);
        }

        [Fact]
        public void ImportCsvBankTransactions_ReturnsOk()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);
            _importManagerMock.Setup(x => x.ImportCsvBankTransactions(It.IsAny<TransactionImportDto>())).Returns(true);

            var result = _importController.ImportCsvBankTransactions(new TransactionImportViewModel());

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void ImportCsvBankTransactions_ReturnsBadRequest_OnException()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);
            _importManagerMock.Setup(x => x.ImportCsvBankTransactions(It.IsAny<TransactionImportDto>())).Throws(new Exception());

            var result = _importController.ImportCsvBankTransactions(new TransactionImportViewModel());

            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
