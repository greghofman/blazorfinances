﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Api.Controllers;
using BlazorFinances.ClientApiShared.ViewModels;
using BlazorFinances.Managers.Interfaces;
using BlazorFinances.Tests.ControllerTests.Base;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace BlazorFinances.Tests.ControllerTests
{
    public class UserGroupsControllerTest : BaseControllerTest
    {
        private Mock<IUserGroupManager> _userGroupManagerMock;
        private UserGroupsController _userGroupsController;

        public UserGroupsControllerTest()
        {
            _userGroupManagerMock = new Mock<IUserGroupManager>();
            _userGroupsController = new UserGroupsController(_userGroupManagerMock.Object, _mockClaimResolverManager.Object);
        }

        [Fact]
        public void Get_ReturnsOk()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);
            _userGroupManagerMock.Setup(x => x.Get(It.IsAny<string>())).Returns(new UserGroupDto());

            var result = _userGroupsController.Get();

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Get_ReturnsBadRequest_OnException()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);
            _userGroupManagerMock.Setup(x => x.Get(It.IsAny<string>())).Throws(new Exception());

            var result = _userGroupsController.Get();

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task Create_ReturnsOk()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);
            _userGroupManagerMock.Setup(x => x.Create(It.IsAny<UserGroupDto>(), It.IsAny<string>())).Returns(Task.FromResult(1));

            var result = await _userGroupsController.Create(new UserGroupViewModel());

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task Create_ReturnsBadRequest_OnException()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);
            _userGroupManagerMock.Setup(x => x.Create(It.IsAny<UserGroupDto>(), It.IsAny<string>())).Throws(new Exception());

            var result = await _userGroupsController.Create(new UserGroupViewModel());

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task Edit_ReturnsOk()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);
            _userGroupManagerMock.Setup(x => x.Edit(It.IsAny<string>(), It.IsAny<UserGroupEditDto>())).Returns(Task.FromResult("SomeEdit"));

            var result = await _userGroupsController.Edit(new UserGroupEditViewModel());

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task Edit_ReturnsBadRequest_OnException()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);
            _userGroupManagerMock.Setup(x => x.Edit(It.IsAny<string>(), It.IsAny<UserGroupEditDto>())).Throws(new Exception());

            var result = await _userGroupsController.Edit(new UserGroupEditViewModel());

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task Delete_ReturnsOk()
        {
            _userGroupManagerMock.Setup(x => x.DeleteGroupMember(It.IsAny<string>())).Returns(Task.FromResult("someUserId"));

            var result = await _userGroupsController.Delete("someEmail@test.com");

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task Delete_ReturnsBadRequest_OnException()
        {
            _userGroupManagerMock.Setup(x => x.DeleteGroupMember(It.IsAny<string>())).Throws(new Exception());

            var result = await _userGroupsController.Delete("SomeEmail@test.com");

            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
