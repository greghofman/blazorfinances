﻿using AutoMapper;
using BlazorFinances.Accessors.Models;
using BlazorFinances.Api.Config;
using BlazorFinances.Managers.Interfaces;
using BlazorFinances.Tests.Base;
using Microsoft.AspNetCore.Identity;
using Moq;
using System.Collections.Generic;
using System.Reflection;
using System.Security.Claims;
using System.Security.Principal;

namespace BlazorFinances.Tests.ControllerTests.Base
{
    public class BaseControllerTest : BaseTest
    {
        protected ClaimsPrincipal _identity;
        protected Mock<UserManager<ApplicationUser>> _mockUserManager;
        protected Mock<IClaimResolverManager> _mockClaimResolverManager;

        protected BaseControllerTest() 
        {
            //setup fake identity/claims for controllers to get userid from
            var genericIdentity = new GenericIdentity("someFakeUser");
            genericIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, "Some-User-Id-Guid"));
            _identity = new ClaimsPrincipal(genericIdentity);

            //initialize mock user manager
            _mockUserManager = GetMockUserManager<ApplicationUser>(new List<ApplicationUser>());

            //initialize mock claim resolver
            _mockClaimResolverManager = new Mock<IClaimResolverManager>(MockBehavior.Strict);
        }

        private Mock<UserManager<TUser>> GetMockUserManager<TUser>(List<TUser> users) where TUser : class
        {
            var store = new Mock<IUserStore<TUser>>();
            var mgr = new Mock<UserManager<TUser>>(store.Object, null, null, null, null, null, null, null, null);
            mgr.Object.UserValidators.Add(new UserValidator<TUser>());
            mgr.Object.PasswordValidators.Add(new PasswordValidator<TUser>());

            mgr.Setup(x => x.DeleteAsync(It.IsAny<TUser>())).ReturnsAsync(IdentityResult.Success);
            mgr.Setup(x => x.CreateAsync(It.IsAny<TUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success)
                .Callback<TUser, string>((x, y) => users.Add(x));
            mgr.Setup(x => x.UpdateAsync(It.IsAny<TUser>())).ReturnsAsync(IdentityResult.Success);
            mgr.Setup(x => x.GenerateEmailConfirmationTokenAsync(It.IsAny<TUser>())).ReturnsAsync("someEmailConfirmToken");
            mgr.Setup(x => x.GeneratePasswordResetTokenAsync(It.IsAny<TUser>())).ReturnsAsync("somePasswordResetToken");

            return mgr;
        }
    }
}
