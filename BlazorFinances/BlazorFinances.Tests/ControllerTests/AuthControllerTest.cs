﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Models;
using BlazorFinances.Api.Controllers;
using BlazorFinances.ClientApiShared.ViewModels.Account;
using BlazorFinances.Common.Types;
using BlazorFinances.Managers.Interfaces;
using BlazorFinances.Tests.ControllerTests.Base;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace BlazorFinances.Tests.ControllerTests
{
    public class AuthControllerTest : BaseControllerTest
    {
        private readonly Mock<IJwtTokenManager> _tokenManagerMock;
        private readonly Mock<IEmailManager> _emailManagerMock;
        private readonly Mock<IConfiguration> _configMock;
        private AuthController _authController;

        public AuthControllerTest() : base()
        {
            _tokenManagerMock = new Mock<IJwtTokenManager>();
            _emailManagerMock = new Mock<IEmailManager>();
            _tokenManagerMock = new Mock<IJwtTokenManager>();
            _configMock = new Mock<IConfiguration>();
            _authController = new AuthController(_tokenManagerMock.Object, _mockUserManager.Object, _emailManagerMock.Object, _mockClaimResolverManager.Object, _configMock.Object);
        }

        [Fact]
        public async Task Register_ReturnsOk()
        {
            _emailManagerMock.Setup(x => x.SendAsync(It.IsAny<EmailType>(), It.IsAny<string>(), It.IsAny<ApplicationUserDto>())).Returns(Task.FromResult(true));

            var vm = new TokenViewModel
            {
                Email = "someEmail@test.com",
                Password = "abc1234567!"
            };

            var result = await _authController.Register(vm);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task Register_ReturnsBadRequest_OnException()
        {
            _emailManagerMock.Setup(x => x.SendAsync(It.IsAny<EmailType>(), It.IsAny<string>(), It.IsAny<ApplicationUserDto>())).ThrowsAsync(new Exception());

            var vm = new TokenViewModel
            {
                Email = "someEmail@test.com",
                Password = "abc1234567!"
            };

            var result = await _authController.Register(vm);
            
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task Login_ReturnsOk()
        {
            _mockUserManager.Setup(x => x.CheckPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).Returns(Task.FromResult(true));
            _mockUserManager.Setup(x => x.FindByEmailAsync(It.IsAny<string>())).Returns(Task.FromResult(new ApplicationUser { EmailConfirmed = true }));
            _tokenManagerMock.Setup(x => x.BuildJwtToken(It.IsAny<string>(), It.IsAny<string>())).Returns("some-jwt-token");

            var vm = new TokenViewModel
            {
                Email = "someEmail@test.com",
                Password = "abc1234567!"
            };

            var result = await _authController.Login(vm);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task Login_ReturnsBadRequest_OnException()
        {
            _mockUserManager.Setup(x => x.CheckPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ThrowsAsync(new Exception());
            _mockUserManager.Setup(x => x.FindByEmailAsync(It.IsAny<string>())).Returns(Task.FromResult(new ApplicationUser { EmailConfirmed = true }));
            _tokenManagerMock.Setup(x => x.BuildJwtToken(It.IsAny<string>(), It.IsAny<string>())).Returns("some-jwt-token");

            var vm = new TokenViewModel
            {
                Email = "someEmail@test.com",
                Password = "abc1234567!"
            };

            var result = await _authController.Login(vm);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task Login_ReturnsBadRequest_OnWrongPassword()
        {
            _mockUserManager.Setup(x => x.CheckPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).Returns(Task.FromResult(false));
            _mockUserManager.Setup(x => x.FindByEmailAsync(It.IsAny<string>())).Returns(Task.FromResult(new ApplicationUser { EmailConfirmed = true }));
            _tokenManagerMock.Setup(x => x.BuildJwtToken(It.IsAny<string>(), It.IsAny<string>())).Returns("some-jwt-token");

            var vm = new TokenViewModel
            {
                Email = "someEmail@test.com",
                Password = "abc1234567!"
            };

            var result = await _authController.Login(vm);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task Login_ReturnsBadRequest_OnEmailUnconfirmed()
        {
            _mockUserManager.Setup(x => x.CheckPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).Returns(Task.FromResult(true));
            _mockUserManager.Setup(x => x.FindByEmailAsync(It.IsAny<string>())).Returns(Task.FromResult(new ApplicationUser()));
            _tokenManagerMock.Setup(x => x.BuildJwtToken(It.IsAny<string>(), It.IsAny<string>())).Returns("some-jwt-token");

            var vm = new TokenViewModel
            {
                Email = "someEmail@test.com",
                Password = "abc1234567!"
            };

            var result = await _authController.Login(vm);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task ConfirmEmail_ReturnsOk()
        {
            _mockUserManager.Setup(x => x.FindByIdAsync(It.IsAny<string>())).Returns(Task.FromResult(new ApplicationUser()));
            _mockUserManager.Setup(x => x.ConfirmEmailAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).Returns(Task.FromResult(IdentityResult.Success));

            var vm = new ConfirmEmailViewModel
            {
                UserId = "someEmail@test.com",
                Token = "some-jwt-token"
            };

            var result = await _authController.ConfirmEmail(vm);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task ConfirmEmail_ReturnsBadRequest_OnException()
        {
            _mockUserManager.Setup(x => x.FindByIdAsync(It.IsAny<string>())).Throws(new Exception());
            _mockUserManager.Setup(x => x.ConfirmEmailAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).Returns(Task.FromResult(IdentityResult.Success));

            var vm = new ConfirmEmailViewModel
            {
                UserId = "someEmail@test.com",
                Token = "some-jwt-token"
            };

            var result = await _authController.ConfirmEmail(vm);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task ConfirmEmail_ReturnsBadRequest_OnConfirmFailure()
        {
            _mockUserManager.Setup(x => x.FindByIdAsync(It.IsAny<string>())).Returns(Task.FromResult(new ApplicationUser()));
            _mockUserManager.Setup(x => x.ConfirmEmailAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).Returns(Task.FromResult(IdentityResult.Failed()));

            var vm = new ConfirmEmailViewModel
            {
                UserId = "someEmail@test.com",
                Token = "some-jwt-token"
            };

            var result = await _authController.ConfirmEmail(vm);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task ResetPassword_ReturnsOk()
        {
            _mockUserManager.Setup(x => x.FindByIdAsync(It.IsAny<string>())).Returns(Task.FromResult(new ApplicationUser()));
            _mockUserManager.Setup(x => x.ResetPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(IdentityResult.Success));

            var vm = new PasswordResetViewModel
            {
                UserId = "someEmail@test.com",
                Token = "some-jwt-token",
                Password = "somePassword"
            };

            var result = await _authController.ResetPassword(vm);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task ResetPassword_ReturnsBadRequest_OnException()
        {
            _mockUserManager.Setup(x => x.FindByIdAsync(It.IsAny<string>())).Throws(new Exception());
            _mockUserManager.Setup(x => x.ResetPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(IdentityResult.Success));

            var vm = new PasswordResetViewModel
            {
                UserId = "someEmail@test.com",
                Token = "some-jwt-token",
                Password = "somePassword"
            };

            var result = await _authController.ResetPassword(vm);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task ResetPassword_ReturnsBadRequest_OnResetFailure()
        {
            _mockUserManager.Setup(x => x.FindByIdAsync(It.IsAny<string>())).Returns(Task.FromResult(new ApplicationUser()));
            _mockUserManager.Setup(x => x.ResetPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(IdentityResult.Failed()));

            var vm = new PasswordResetViewModel
            {
                UserId = "someEmail@test.com",
                Token = "some-jwt-token",
                Password = "somePassword"
            };

            var result = await _authController.ResetPassword(vm);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task ForgotPassword_ReturnsOk()
        {
            _mockUserManager.Setup(x => x.FindByEmailAsync(It.IsAny<string>())).Returns(Task.FromResult(new ApplicationUser()));
            _emailManagerMock.Setup(x => x.SendAsync(It.IsAny<EmailType>(), It.IsAny<string>(), It.IsAny<ApplicationUserDto>())).Returns(Task.FromResult(true));

            var result = await _authController.ForgotPassword("someUser@test.com");

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task ForgotPassword_ReturnsBadRequest_OnException()
        {
            _mockUserManager.Setup(x => x.FindByEmailAsync(It.IsAny<string>())).Throws(new Exception());
            _emailManagerMock.Setup(x => x.SendAsync(It.IsAny<EmailType>(), It.IsAny<string>(), It.IsAny<ApplicationUserDto>())).Returns(Task.FromResult(true));

            var result = await _authController.ForgotPassword("someUser@test.com");

            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
