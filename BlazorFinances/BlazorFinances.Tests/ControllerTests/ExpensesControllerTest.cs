﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.ClientApiShared.ViewModels;
using BlazorFinances.Managers.Interfaces;
using BlazorFinances.Server.Controllers;
using BlazorFinances.Tests.ControllerTests.Base;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace BlazorFinances.Tests.ControllerTests
{
    public class ExpensesControllerTest : BaseControllerTest
    {
        private Mock<IExpenseManager> _expenseManagerMock;
        private ExpensesController _expensesController;

        public ExpensesControllerTest()
        {
            _expenseManagerMock = new Mock<IExpenseManager>();
            _expensesController = new ExpensesController(_expenseManagerMock.Object, _mockClaimResolverManager.Object);
        }

        [Fact]
        public async Task Get_ReturnsOk()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);
            _expenseManagerMock.Setup(x => x.GetAll(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Returns(Task.FromResult(new ExpensesIndexDto()));

            var result = await _expensesController.Get("someSortOrder", "someSearchTerm", 1);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task Get_ReturnsBadRequest_OnException()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);
            _expenseManagerMock.Setup(x => x.GetAll(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Throws(new Exception());

            var result = await _expensesController.Get("someSortOrder", "someSearchTerm", 1);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Get_ById__ReturnsOk()
        {
            _expenseManagerMock.Setup(x => x.Get(1)).Returns(new ExpenseDto());

            var result = _expensesController.Get(1);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Get_ById_ReturnsBadRequest_OnException()
        {
            _expenseManagerMock.Setup(x => x.Get(1)).Throws(new Exception());

            var result = _expensesController.Get(1);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Create__ReturnsOk()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);
            _expenseManagerMock.Setup(x => x.Create(It.IsAny<ExpenseDto>())).Returns(1);

            var result = _expensesController.Create(new ExpenseViewModel());

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Create_ReturnsBadRequest_OnException()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);
            _expenseManagerMock.Setup(x => x.Create(It.IsAny<ExpenseDto>())).Throws(new Exception());

            var result = _expensesController.Create(new ExpenseViewModel());

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Edit__ReturnsOk()
        {
            _expenseManagerMock.Setup(x => x.Edit(It.IsAny<ExpenseDto>())).Returns(1);

            var result = _expensesController.Edit(new ExpenseViewModel());

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Edit_ReturnsBadRequest_OnException()
        {
            _expenseManagerMock.Setup(x => x.Edit(It.IsAny<ExpenseDto>())).Throws(new Exception());

            var result = _expensesController.Edit(new ExpenseViewModel());

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Delete__ReturnsOk()
        {
            _expenseManagerMock.Setup(x => x.Delete(It.IsAny<int>())).Returns(1);

            var result = _expensesController.Delete(1);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Delete_ReturnsBadRequest_OnException()
        {
            _expenseManagerMock.Setup(x => x.Delete(It.IsAny<int>())).Throws(new Exception());

            var result = _expensesController.Delete(1);

            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
