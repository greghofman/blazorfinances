﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Api.Controllers;
using BlazorFinances.Managers.Interfaces;
using BlazorFinances.Tests.ControllerTests.Base;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace BlazorFinances.Tests.ControllerTests
{
    public class AnalyticsControllerTest : BaseControllerTest
    {
        private Mock<IGraphManager> _mockGraphManager;
        private AnalyticsController _analyticsController;

        public AnalyticsControllerTest() : base()
        {
            _mockGraphManager = new Mock<IGraphManager>(MockBehavior.Strict);
            _analyticsController = new AnalyticsController(_mockGraphManager.Object, _mockClaimResolverManager.Object);
        }

        [Fact]
        public async Task GetExpensesEarningsBarChart_ReturnsOk()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);

            _mockGraphManager
                .Setup(x => x.BuildExpensesEarningsBarChart(It.IsAny<string>()))
                .Returns(Task.FromResult(new BarChartDto()));

            var result = await _analyticsController.GetExpensesEarningsBarChart();

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task GetExpensesEarningsBarChart_ReturnsBadRequest_OnException()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);

            _mockGraphManager
                .Setup(x => x.BuildExpensesEarningsBarChart(It.IsAny<string>()))
                .ThrowsAsync(new Exception());

            var result = await _analyticsController.GetExpensesEarningsBarChart();

            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
