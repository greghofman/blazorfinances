﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Api.Controllers;
using BlazorFinances.ClientApiShared.ViewModels;
using BlazorFinances.Managers.Interfaces;
using BlazorFinances.Tests.ControllerTests.Base;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace BlazorFinances.Tests.ControllerTests
{
    public class EarningsControllerTest : BaseControllerTest
    {
        private Mock<IEarningManager> _earningManagerMock;
        private EarningsController _earningsController;

        public EarningsControllerTest()
        {
            _earningManagerMock = new Mock<IEarningManager>();
            _earningsController = new EarningsController(_earningManagerMock.Object, _mockClaimResolverManager.Object);
        }

        [Fact]
        public async Task Get_ReturnsOk()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);
            _earningManagerMock.Setup(x => x.GetAll(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Returns(Task.FromResult(new EarningsIndexDto()));

            var result = await _earningsController.Get("someSortOrder", "someSearchTerm", 1);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task Get_ReturnsBadRequest_OnException()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);
            _earningManagerMock.Setup(x => x.GetAll(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Throws(new Exception());

            var result = await _earningsController.Get("someSortOrder", "someSearchTerm", 1);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Get_ById__ReturnsOk()
        {
            _earningManagerMock.Setup(x => x.Get(1)).Returns(new EarningDto());

            var result = _earningsController.Get(1);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Get_ById_ReturnsBadRequest_OnException()
        {
            _earningManagerMock.Setup(x => x.Get(1)).Throws(new Exception());

            var result = _earningsController.Get(1);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Create__ReturnsOk()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);
            _earningManagerMock.Setup(x => x.Create(It.IsAny<EarningDto>())).Returns(1);

            var result = _earningsController.Create(new EarningViewModel());

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Create_ReturnsBadRequest_OnException()
        {
            _mockClaimResolverManager.Setup(x => x.User).Returns(_identity);
            _earningManagerMock.Setup(x => x.Create(It.IsAny<EarningDto>())).Throws(new Exception());

            var result = _earningsController.Create(new EarningViewModel());

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Edit__ReturnsOk()
        {
            _earningManagerMock.Setup(x => x.Edit(It.IsAny<EarningDto>())).Returns(1);

            var result = _earningsController.Edit(new EarningViewModel());

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Edit_ReturnsBadRequest_OnException()
        {
            _earningManagerMock.Setup(x => x.Edit(It.IsAny<EarningDto>())).Throws(new Exception());

            var result = _earningsController.Edit(new EarningViewModel());

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Delete__ReturnsOk()
        {
            _earningManagerMock.Setup(x => x.Delete(It.IsAny<int>())).Returns(1);

            var result = _earningsController.Delete(1);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Delete_ReturnsBadRequest_OnException()
        {
            _earningManagerMock.Setup(x => x.Delete(It.IsAny<int>())).Throws(new Exception());

            var result = _earningsController.Delete(1);

            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
