﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Api.Controllers;
using BlazorFinances.ClientApiShared.ViewModels;
using BlazorFinances.Managers.Interfaces;
using BlazorFinances.Tests.ControllerTests.Base;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace BlazorFinances.Tests.ControllerTests
{
    public class CategoriesControllerTest : BaseControllerTest
    {
        private Mock<ICategoryManager> _categoriesManagerMock;
        private CategoriesController _categoriesController;

        public CategoriesControllerTest()
        {
            _categoriesManagerMock = new Mock<ICategoryManager>();
            _categoriesController = new CategoriesController(_categoriesManagerMock.Object, _mockClaimResolverManager.Object);
        }

        [Fact]
        public void Get_ReturnsOk()
        {
            _categoriesManagerMock.Setup(x => x.GetAll()).Returns(new List<CategoryDto>());

            var result = _categoriesController.Get();

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Get_ReturnsBadRequest_OnException()
        {
            _categoriesManagerMock.Setup(x => x.GetAll()).Throws(new Exception());

            var result = _categoriesController.Get();

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Get_ById_ReturnsOk()
        {
            _categoriesManagerMock.Setup(x => x.Get(1)).Returns(new CategoryDto());

            var result = _categoriesController.Get(1);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Get_ById_ReturnsBadRequest_OnException()
        {
            _categoriesManagerMock.Setup(x => x.Get(1)).Throws(new Exception());

            var result = _categoriesController.Get(1);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Create_ReturnsOk()
        {
            _categoriesManagerMock.Setup(x => x.Create(It.IsAny<CategoryDto>())).Returns(1);

            var result = _categoriesController.Create(new CategoryViewModel());

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Create_ReturnsBadRequest_OnException()
        {
            _categoriesManagerMock.Setup(x => x.Create(It.IsAny<CategoryDto>())).Throws(new Exception());

            var result = _categoriesController.Create(new CategoryViewModel());

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Edit_ReturnsOk()
        {
            _categoriesManagerMock.Setup(x => x.Edit(It.IsAny<CategoryDto>()));

            var result = _categoriesController.Edit(new CategoryViewModel());

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Edit_ReturnsBadRequest_OnException()
        {
            _categoriesManagerMock.Setup(x => x.Edit(It.IsAny<CategoryDto>())).Throws(new Exception());

            var result = _categoriesController.Edit(new CategoryViewModel());

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void Delete_ReturnsOk()
        {
            _categoriesManagerMock.Setup(x => x.Delete(It.IsAny<int>()));

            var result = _categoriesController.Delete(1);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Delete_ReturnsBadRequest_OnException()  
        {
            _categoriesManagerMock.Setup(x => x.Delete(It.IsAny<int>())).Throws(new Exception());

            var result = _categoriesController.Delete(1);

            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
