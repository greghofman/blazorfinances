﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Engines.Engines;
using BlazorFinances.Tests.Base;
using Moq;
using System.Collections.Generic;
using Xunit;

namespace BlazorFinances.Tests.EngineTests
{
    public class BarChartEngineTest : BaseTest
    {
        public class BankDataImportEngineTest : BaseTest
        {
            private BarChartEngine _barChartEngine;

            public BankDataImportEngineTest()
            {
                _barChartEngine = new BarChartEngine();
            }
            [Fact]
            public void BuildExpenseEarningBarChart_ReturnsDto()
            {
                var result = _barChartEngine.BuildExpenseEarningBarChart(new List<ExpenseDto>(), new List<EarningDto>());

                Assert.IsType<BarChartDto>(result);
            }
        }
    }
}
