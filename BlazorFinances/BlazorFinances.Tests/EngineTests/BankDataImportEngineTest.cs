using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Engines.Engines;
using BlazorFinances.Tests.Base;
using Microsoft.AspNetCore.Http;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace BlazorFinances.Tests.EngineTests
{
    public class BankDataImportEngineTest : BaseTest
    {
        private Mock<IExpenseAccessor> _expenseAccessorMock;
        private Mock<IEarningAccessor> _earningAccessorMock;
        private BankDataImportEngine _bankDataImportEngine;

        public BankDataImportEngineTest()
        {
            _expenseAccessorMock = new Mock<IExpenseAccessor>();
            _earningAccessorMock = new Mock<IEarningAccessor>();
            _bankDataImportEngine = new BankDataImportEngine(_earningAccessorMock.Object, _expenseAccessorMock.Object);
        }

        [Fact]
        public async Task GetFileContents_ReturnsContent()
        {
            var fileMock = new Mock<IFormFile>();
            var content = "Hello World from a Fake File";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);

            var result = await _bankDataImportEngine.GetFileContents(fileMock.Object);

            Assert.IsType<string>(result);
        }

        [Fact]
        public void ImportUsBankTransactionCsvData_ReturnsSuccess()
        {
            _earningAccessorMock.Setup(x => x.CreateAll(It.IsAny<List<EarningDto>>())).Returns(new List<int>());
            _expenseAccessorMock.Setup(x => x.CreateAll(It.IsAny<List<ExpenseDto>>())).Returns(new List<int>());

            var result = _bankDataImportEngine.ImportUsBankTransactionCsvData("someStringContent", "someUserId");

            Assert.IsType<bool>(result);
            Assert.True(result);
        }
    }
}
