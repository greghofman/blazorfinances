﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Engines.Engines;
using BlazorFinances.Engines.Interfaces;
using BlazorFinances.Tests.Base;
using Moq;
using System.Collections.Generic;
using Xunit;

namespace BlazorFinances.Tests.EngineTests
{
    public class EarningEngineTest : BaseTest
    {
        private Mock<IUserGroupAccessor> _userGroupAccessorMock;
        private Mock<IEarningAccessor> _earningAccessorMock;
        private EarningEngine _earningEngine;

        public EarningEngineTest()
        {
            _userGroupAccessorMock = new Mock<IUserGroupAccessor>();
            _earningAccessorMock = new Mock<IEarningAccessor>();
            _earningEngine = new EarningEngine(_earningAccessorMock.Object, _userGroupAccessorMock.Object);
        }

        [Fact]
        public void GetEarningsForGroup_SingleUser_ReturnsDtos()
        {
            _earningAccessorMock.Setup(x => x.GetAll(It.IsAny<string>())).Returns(new List<EarningDto>());
            var result = _earningEngine.GetEarningsForGroup(new ApplicationUserDto());

            Assert.IsType<List<EarningDto>>(result);
        }

        [Fact]
        public void GetEarningsForGroup_MultipleUsers_ReturnsDtos()
        {
            _earningAccessorMock.Setup(x => x.GetAll(It.IsAny<List<string>>())).Returns(new List<EarningDto>());
            _userGroupAccessorMock.Setup(x => x.Get(It.IsAny<int>()))
                .Returns(new UserGroupDto
                {
                    Users = new List<ApplicationUserDto>
                    {
                        new ApplicationUserDto { Id = "someUserId" }
                    }
                });

            var result = _earningEngine.GetEarningsForGroup(new ApplicationUserDto { GroupId = 1 });

            Assert.IsType<List<EarningDto>>(result);
        }

        [Fact]
        public void GetEarningsForIndex_SingleUser_ReturnsDtos()
        {
            _earningAccessorMock.Setup(x => x.GetAllForIndex(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Returns(new EarningsIndexDto());
            var result = _earningEngine.GetEarningsForIndex(new ApplicationUserDto(), "someSortOrder", "someSearchTerm", 1);

            Assert.IsType<EarningsIndexDto>(result);
        }

        [Fact]
        public void GetEarningsForIndex_MultipleUsers_ReturnsDtos()
        {
            _earningAccessorMock.Setup(x => x.GetAllForIndex(It.IsAny<List<string>>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Returns(new EarningsIndexDto());
            _userGroupAccessorMock.Setup(x => x.Get(It.IsAny<int>()))
                .Returns(new UserGroupDto
                {
                    Users = new List<ApplicationUserDto>
                    {
                        new ApplicationUserDto { Id = "someUserId" }
                    }
                });

            var result = _earningEngine.GetEarningsForIndex(new ApplicationUserDto { GroupId = 1 }, "someSortOrder", "someSearchTerm", 1);

            Assert.IsType<EarningsIndexDto>(result);
        }
    }
}
