﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Engines.Engines;
using BlazorFinances.Tests.Base;
using Moq;
using System.Collections.Generic;
using Xunit;

namespace BlazorFinances.Tests.EngineTests
{
    public class ExpenseEngineTest : BaseTest
    {
        private Mock<IUserGroupAccessor> _userGroupAccessorMock;
        private Mock<IExpenseAccessor> _expenseAccessorMock;
        private ExpenseEngine _expenseEngine;

        public ExpenseEngineTest()
        {
            _userGroupAccessorMock = new Mock<IUserGroupAccessor>();
            _expenseAccessorMock = new Mock<IExpenseAccessor>();
            _expenseEngine = new ExpenseEngine(_userGroupAccessorMock.Object, _expenseAccessorMock.Object);
        }

        [Fact]
        public void GetExpensesForGroup_SingleUser_ReturnsDtos()
        {
            _expenseAccessorMock.Setup(x => x.GetAll(It.IsAny<string>())).Returns(new List<ExpenseDto>());
            var result = _expenseEngine.GetExpensesForGroup(new ApplicationUserDto());

            Assert.IsType<List<ExpenseDto>>(result);
        }

        [Fact]
        public void GetExpensesForGroup_MultipleUsers_ReturnsDtos()
        {
            _expenseAccessorMock.Setup(x => x.GetAll(It.IsAny<List<string>>())).Returns(new List<ExpenseDto>());
            _userGroupAccessorMock.Setup(x => x.Get(It.IsAny<int>()))
                .Returns(new UserGroupDto
                {
                    Users = new List<ApplicationUserDto>
                    {
                        new ApplicationUserDto { Id = "someUserId" }
                    }
                });

            var result = _expenseEngine.GetExpensesForGroup(new ApplicationUserDto { GroupId = 1 });

            Assert.IsType<List<ExpenseDto>>(result);
        }

        [Fact]
        public void GetExpensesForIndex_SingleUser_ReturnsDtos()
        {
            _expenseAccessorMock.Setup(x => x.GetAllForIndex(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Returns(new ExpensesIndexDto());
            var result = _expenseEngine.GetExpensesForIndex(new ApplicationUserDto(), "someSortOrder", "someSearchTerm", 1);

            Assert.IsType<ExpensesIndexDto>(result);
        }

        [Fact]
        public void GetExpensesForIndex_MultipleUsers_ReturnsDtos()
        {
            _expenseAccessorMock.Setup(x => x.GetAllForIndex(It.IsAny<List<string>>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Returns(new ExpensesIndexDto());
            _userGroupAccessorMock.Setup(x => x.Get(It.IsAny<int>()))
                .Returns(new UserGroupDto
                {
                    Users = new List<ApplicationUserDto>
                    {
                        new ApplicationUserDto { Id = "someUserId" }
                    }
                });

            var result = _expenseEngine.GetExpensesForIndex(new ApplicationUserDto { GroupId = 1 }, "someSortOrder", "someSearchTerm", 1);

            Assert.IsType<ExpensesIndexDto>(result);
        }
    }
}
