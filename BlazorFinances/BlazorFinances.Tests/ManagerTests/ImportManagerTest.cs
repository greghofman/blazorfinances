﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Engines.Interfaces;
using BlazorFinances.Managers.Managers;
using BlazorFinances.Tests.ManagerTests.Base;
using Moq;
using Xunit;

namespace BlazorFinances.Tests.ManagerTests
{
    public class ImportManagerTest : BaseManagerTest
    {
        private Mock<IBankDataImportEngine> _bankImportEngineMock;
        private ImportManager _importManager;

        public ImportManagerTest()
        {
            _bankImportEngineMock = new Mock<IBankDataImportEngine>();
            _importManager = new ImportManager(_bankImportEngineMock.Object);
        }

        [Fact]
        public void BuildExpensesEarningsBarChart_ReturnsDto()
        {
            _bankImportEngineMock.Setup(x => x.ImportUsBankTransactionCsvData(It.IsAny<string>(), It.IsAny<string>())).Returns(true);

            var result = _importManager.ImportCsvBankTransactions(new TransactionImportDto { Base64FileContents = string.Empty });

            Assert.IsType<bool>(result);
            Assert.True(result);
        }
    }
}
