﻿using BlazorFinances.Accessors.Models;
using BlazorFinances.Tests.Base;
using Microsoft.AspNetCore.Identity;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace BlazorFinances.Tests.ManagerTests.Base
{
    public class BaseManagerTest : BaseTest
    {
        protected Mock<UserManager<ApplicationUser>> _mockUserManager;

        public BaseManagerTest()
        {
            _mockUserManager = GetMockUserManager<ApplicationUser>(new List<ApplicationUser>()
            {
                new ApplicationUser
                {
                    Id = "someUserId",
                    Group = new UserGroup { Id = 1}
                }
            });
        }

        private Mock<UserManager<TUser>> GetMockUserManager<TUser>(List<TUser> users) where TUser : class
        {
            var store = new Mock<IUserStore<TUser>>();
            var mgr = new Mock<UserManager<TUser>>(store.Object, null, null, null, null, null, null, null, null);
            
            mgr.Object.UserValidators.Add(new UserValidator<TUser>());
            mgr.Object.PasswordValidators.Add(new PasswordValidator<TUser>());

            mgr.Setup(x => x.Users).Returns(users.AsQueryable());
            mgr.Setup(x => x.DeleteAsync(It.IsAny<TUser>())).ReturnsAsync(IdentityResult.Success);
            mgr.Setup(x => x.CreateAsync(It.IsAny<TUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success)
                .Callback<TUser, string>((x, y) => users.Add(x));
            mgr.Setup(x => x.UpdateAsync(It.IsAny<TUser>())).ReturnsAsync(IdentityResult.Success);
            mgr.Setup(x => x.GenerateEmailConfirmationTokenAsync(It.IsAny<TUser>())).ReturnsAsync("someEmailConfirmToken");
            mgr.Setup(x => x.GeneratePasswordResetTokenAsync(It.IsAny<TUser>())).ReturnsAsync("somePasswordResetToken");

            return mgr;
        }
    }
}
