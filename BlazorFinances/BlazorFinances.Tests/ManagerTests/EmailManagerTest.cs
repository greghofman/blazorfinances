﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Common.Types;
using BlazorFinances.Engines.Interfaces;
using BlazorFinances.Managers.Managers;
using Moq;
using System.Net.Mail;
using System.Threading.Tasks;
using Xunit;

namespace BlazorFinances.Tests.ManagerTests
{
    public class EmailManagerTest
    {
        private Mock<IEmailEngine> _emailEngineMock;
        private EmailManager _emailManager;

        public EmailManagerTest()
        {
            _emailEngineMock = new Mock<IEmailEngine>();
            _emailManager = new EmailManager(_emailEngineMock.Object);
        }

        [Fact]
        public async Task SendAsync_ReturnsSuccess()
        {
            _emailEngineMock.Setup(x => x.GetConfirmEmail(It.IsAny<string>(), It.IsAny<ApplicationUserDto>())).Returns(new MailMessage());
            _emailEngineMock.Setup(x => x.SendEmail(It.IsAny<MailMessage>())).Returns(Task.FromResult(true));

            var result = await _emailManager.SendAsync(EmailType.ConfirmEmail, "someCallbackUrl", new ApplicationUserDto());

            Assert.IsType<bool>(result);
            Assert.True(result);
        }
    }
}
