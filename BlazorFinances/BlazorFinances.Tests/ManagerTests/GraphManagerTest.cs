﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Models;
using BlazorFinances.Engines.Interfaces;
using BlazorFinances.Managers.Managers;
using BlazorFinances.Tests.ManagerTests.Base;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace BlazorFinances.Tests.ManagerTests
{
    public class GraphManagerTest : BaseManagerTest
    {
        private Mock<IBarChartEngine> _barChartEngineMock;
        private Mock<IPieChartEngine> _pieChartEngineMock;
        private Mock<ILineChartEngine> _lineChartEngineMock;
        private Mock<IEarningEngine> _earningEngineMock;
        private Mock<IExpenseEngine> _expensesEngineMock;
        private GraphManager _graphManager;

        public GraphManagerTest()
        {
            _barChartEngineMock = new Mock<IBarChartEngine>();
            _pieChartEngineMock = new Mock<IPieChartEngine>();
            _lineChartEngineMock = new Mock<ILineChartEngine>();
            _earningEngineMock = new Mock<IEarningEngine>();
            _expensesEngineMock = new Mock<IExpenseEngine>();
            _graphManager = new GraphManager(_barChartEngineMock.Object, _pieChartEngineMock.Object, _lineChartEngineMock.Object, _expensesEngineMock.Object, _earningEngineMock.Object, _mockUserManager.Object);
        }

        [Fact]
        public async Task BuildExpensesEarningsBarChart_ReturnsDto()
        {
            _mockUserManager.Setup(x => x.FindByIdAsync(It.IsAny<string>())).Returns(Task.FromResult(new ApplicationUser()));
            _earningEngineMock.Setup(x => x.GetEarningsForGroup(It.IsAny<ApplicationUserDto>())).Returns(new List<EarningDto>());
            _expensesEngineMock.Setup(x => x.GetExpensesForGroup(It.IsAny<ApplicationUserDto>())).Returns(new List<ExpenseDto>());
            _barChartEngineMock.Setup(x => x.BuildExpenseEarningBarChart(It.IsAny<List<ExpenseDto>>(), It.IsAny<List<EarningDto>>())).Returns(new BarChartDto());

            var result = await _graphManager.BuildExpensesEarningsBarChart("someUserId");

            Assert.IsType<BarChartDto>(result);
        }
    }
}
