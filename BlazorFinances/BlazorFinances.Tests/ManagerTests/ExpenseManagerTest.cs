﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Accessors.Models;
using BlazorFinances.Engines.Interfaces;
using BlazorFinances.Managers.Managers;
using BlazorFinances.Tests.ManagerTests.Base;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace BlazorFinances.Tests.ManagerTests
{
    public class ExpenseManagerTest : BaseManagerTest
    {
        private Mock<IExpenseAccessor> _expensesAccessorMock;
        private Mock<IExpenseEngine> _expensesEngineMock;
        private ExpenseManager _expenseManager;

        public ExpenseManagerTest()
        {
            _expensesAccessorMock = new Mock<IExpenseAccessor>();
            _expensesEngineMock = new Mock<IExpenseEngine>();
            _expenseManager = new ExpenseManager(_expensesAccessorMock.Object, _expensesEngineMock.Object, _mockUserManager.Object);
        }

        [Fact]
        public void Create_ReturnsId()
        {
            _expensesAccessorMock.Setup(x => x.Create(It.IsAny<ExpenseDto>())).Returns(1);

            var result = _expenseManager.Create(new ExpenseDto());

            Assert.IsType<int>(result);
        }

        [Fact]
        public void Delete_ReturnsId()
        {
            _expensesAccessorMock.Setup(x => x.Delete(It.IsAny<int>())).Returns(1);

            var result = _expenseManager.Delete(1);

            Assert.IsType<int>(result);
        }

        [Fact]
        public void Edit_ReturnsId()
        {
            _expensesAccessorMock.Setup(x => x.Update(It.IsAny<ExpenseDto>())).Returns(1);

            var result = _expenseManager.Edit(new ExpenseDto());

            Assert.IsType<int>(result);
        }

        [Fact]
        public void Get_ReturnsDto()
        {
            _expensesAccessorMock.Setup(x => x.Get(It.IsAny<int>())).Returns(new ExpenseDto());

            var result = _expenseManager.Get(1);

            Assert.IsType<ExpenseDto>(result);
        }

        [Fact]
        public async Task GetAll_ReturnsDtos()
        {
            _expensesEngineMock.Setup(x => x.GetExpensesForIndex(It.IsAny<ApplicationUserDto>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Returns(new ExpensesIndexDto());
            _mockUserManager.Setup(x => x.FindByIdAsync(It.IsAny<string>())).Returns(Task.FromResult(new ApplicationUser()));

            var result = await _expenseManager.GetAll("someUserId", "someSortOrder", "someSearchTerm", 1);

            Assert.IsType<ExpensesIndexDto>(result);
        }
    }
}
