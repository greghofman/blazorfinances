﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Accessors.Models;
using BlazorFinances.Engines.Interfaces;
using BlazorFinances.Managers.Managers;
using BlazorFinances.Tests.ManagerTests.Base;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace BlazorFinances.Tests.ManagerTests
{
    public class EarningManagerTest : BaseManagerTest
    {
        private Mock<IEarningAccessor> _earningsAccessorMock;
        private Mock<IEarningEngine> _earningEngineMock;
        private EarningManager _earningManager;

        public EarningManagerTest()
        {
            _earningsAccessorMock = new Mock<IEarningAccessor>();
            _earningEngineMock = new Mock<IEarningEngine>();
            _earningManager = new EarningManager(_earningsAccessorMock.Object, _earningEngineMock.Object, _mockUserManager.Object);
        }
        
        [Fact]
        public void Create_ReturnsId()
        {
            _earningsAccessorMock.Setup(x => x.Create(It.IsAny<EarningDto>())).Returns(1);

            var result = _earningManager.Create(new EarningDto());

            Assert.IsType<int>(result);
        }

        [Fact]
        public void Delete_ReturnsId()
        {
            _earningsAccessorMock.Setup(x => x.Delete(It.IsAny<int>())).Returns(1);

            var result = _earningManager.Delete(1);

            Assert.IsType<int>(result);
        }

        [Fact]
        public void Edit_ReturnsId()
        {
            _earningsAccessorMock.Setup(x => x.Update(It.IsAny<EarningDto>())).Returns(1);

            var result = _earningManager.Edit(new EarningDto());

            Assert.IsType<int>(result);
        }

        [Fact]
        public void Get_ReturnsDto()
        {
            _earningsAccessorMock.Setup(x => x.Get(It.IsAny<int>())).Returns(new EarningDto());

            var result = _earningManager.Get(1);

            Assert.IsType<EarningDto>(result);
        }

        [Fact]
        public async Task GetAll_ReturnsDtos()
        {
            _earningEngineMock.Setup(x => x.GetEarningsForIndex(It.IsAny<ApplicationUserDto>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Returns(new EarningsIndexDto());
            _mockUserManager.Setup(x => x.FindByIdAsync(It.IsAny<string>())).Returns(Task.FromResult(new ApplicationUser()));

            var result = await _earningManager.GetAll("someUserId", "someSortOrder", "someSearchTerm", 1);

            Assert.IsType<EarningsIndexDto>(result);
        }
    }
}
