﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Accessors.Models;
using BlazorFinances.Managers.Managers;
using BlazorFinances.Tests.ManagerTests.Base;
using Microsoft.AspNetCore.Identity;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace BlazorFinances.Tests.ManagerTests
{
    public class UserGroupManagerTest : BaseManagerTest
    {
        private Mock<IUserGroupAccessor> _userGroupAccessorMock;
        private UserGroupManager _userGroupManager;

        public UserGroupManagerTest()
        {
            _userGroupAccessorMock = new Mock<IUserGroupAccessor>();
            _userGroupManager = new UserGroupManager(_userGroupAccessorMock.Object, _mockUserManager.Object);
        }
        
        [Fact]
        public async Task Create_ReturnsId()
        {
            _userGroupAccessorMock.Setup(x => x.Create(It.IsAny<UserGroupDto>())).Returns(1);
            _mockUserManager.Setup(x => x.FindByIdAsync(It.IsAny<string>())).Returns(Task.FromResult(new ApplicationUser()));
            _mockUserManager.Setup(x => x.UpdateAsync(It.IsAny<ApplicationUser>())).Returns(Task.FromResult(IdentityResult.Success));

            var result = await _userGroupManager.Create(new UserGroupDto(), "someUserId");

            Assert.IsType<int>(result);
        }

        [Fact]
        public void Get_ReturnsDto()
        {
            var result = _userGroupManager.Get("someUserId");

            Assert.IsType<UserGroupDto>(result);
        }

        [Fact]
        public async Task Edit_ReturnsId()
        {
            _mockUserManager.Setup(x => x.FindByNameAsync(It.IsAny<string>())).Returns(Task.FromResult(new ApplicationUser()));
            _mockUserManager.Setup(x => x.FindByIdAsync(It.IsAny<string>())).Returns(Task.FromResult(new ApplicationUser()));
            _mockUserManager.Setup(x => x.UpdateAsync(It.IsAny<ApplicationUser>())).Returns(Task.FromResult(IdentityResult.Success));

            var result = await _userGroupManager.Edit("someUserId", new UserGroupEditDto { NewGroupName = string.Empty });

            Assert.IsType<string>(result);
        }

        [Fact]
        public async Task Delete_ReturnsEmail()
        {
            _mockUserManager.Setup(x => x.FindByEmailAsync(It.IsAny<string>())).Returns(Task.FromResult(new ApplicationUser()));
            _mockUserManager.Setup(x => x.UpdateAsync(It.IsAny<ApplicationUser>())).Returns(Task.FromResult(IdentityResult.Success));

            var result = await _userGroupManager.DeleteGroupMember("someEmail@test.com");

            Assert.IsType<string>(result);
        }
    }
}
