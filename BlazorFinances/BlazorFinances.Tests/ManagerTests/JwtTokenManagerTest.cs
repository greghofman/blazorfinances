﻿using BlazorFinances.Managers.Managers;
using BlazorFinances.Tests.ManagerTests.Base;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit;

namespace BlazorFinances.Tests.ManagerTests
{
    public class JwtTokenManagerTest : BaseManagerTest
    {
        private Mock<IConfiguration> _configurationMock;
        private JwtTokenManager _jwtTokenManager;

        public JwtTokenManagerTest()
        {
            _configurationMock = new Mock<IConfiguration>();
            _jwtTokenManager = new JwtTokenManager(_configurationMock.Object);
        }

        [Fact]
        public void BuildJwtToken_ReturnsToken()
        {
            _configurationMock.Setup(x => x["Jwt:Key"]).Returns("1234567412345678");
            _configurationMock.Setup(x => x["Jwt:Issuer"]).Returns("1234567412345678");
            _configurationMock.Setup(x => x["Jwt:Audience"]).Returns("1234567412345678");
            _configurationMock.Setup(x => x["Jwt:ExpireTime"]).Returns("30");

            var result = _jwtTokenManager.BuildJwtToken("someEmail@test.com", "someUserId");

            Assert.IsType<string>(result);
        }
    }
}
