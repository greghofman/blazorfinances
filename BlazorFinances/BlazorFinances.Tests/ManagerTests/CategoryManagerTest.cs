﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Managers.Managers;
using BlazorFinances.Tests.ManagerTests.Base;
using Moq;
using System.Collections.Generic;
using Xunit;

namespace BlazorFinances.Tests.ManagerTests
{
    public class CategoryManagerTest : BaseManagerTest
    {
        private Mock<ICategoryAccessor> _categoriesAccessorMock;
        private CategoryManager _categoryManager;

        public CategoryManagerTest()
        {
            _categoriesAccessorMock = new Mock<ICategoryAccessor>();
            _categoryManager = new CategoryManager(_categoriesAccessorMock.Object);
        }
        
        [Fact]
        public void Create_ReturnsId()
        {
            _categoriesAccessorMock.Setup(x => x.Create(It.IsAny<CategoryDto>())).Returns(1);

            var result = _categoryManager.Create(new CategoryDto());

            Assert.IsType<int>(result);
        }

        [Fact]
        public void Delete_ReturnsId()
        {
            _categoriesAccessorMock.Setup(x => x.Delete(It.IsAny<int>())).Returns(1);

            var result = _categoryManager.Delete(1);

            Assert.IsType<int>(result);
        }

        [Fact]
        public void Edit_ReturnsId()
        {
            _categoriesAccessorMock.Setup(x => x.Update(It.IsAny<CategoryDto>())).Returns(1);

            var result = _categoryManager.Edit(new CategoryDto());

            Assert.IsType<int>(result);
        }

        [Fact]
        public void Get_ReturnsDto()
        {
            _categoriesAccessorMock.Setup(x => x.Get(It.IsAny<int>())).Returns(new CategoryDto());

            var result = _categoryManager.Get(1);

            Assert.IsType<CategoryDto>(result);
        }

        [Fact]
        public void GetAll_ReturnsDtos()
        {
            _categoriesAccessorMock.Setup(x => x.GetAll()).Returns(new List<CategoryDto>());

            var result = _categoryManager.GetAll();

            Assert.IsType<List<CategoryDto>>(result);
        }
    }
}
