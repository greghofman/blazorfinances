﻿using AutoMapper;
using BlazorFinances.Api.Config;

namespace BlazorFinances.Tests.Base
{
    public class BaseTest
    {
        private static object _mapperLock = new object();
        private static bool _isInitialized = false;

        public BaseTest()
        {
            //this lock is needed because the various xUnit test classes will be created in parallel
            //so initialize will error out due to being called more than once
            lock (_mapperLock)
            {
                if (!_isInitialized)
                {
                    _isInitialized = true;

                    Mapper.Initialize(cfg =>
                    {
                        cfg.AddProfile(new ApiMapperConfig());
                    });
                }
            }
        }
    }
}
