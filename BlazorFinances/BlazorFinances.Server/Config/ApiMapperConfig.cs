﻿using AutoMapper;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.ClientApiShared.ViewModels;

namespace BlazorFinances.Api.Config
{
    public class ApiMapperConfig : Profile
    {
        public ApiMapperConfig()
        {
            CreateMap<CategoryViewModel, CategoryDto>();
            CreateMap<CategoryDto, CategoryViewModel>();

            CreateMap<EarningViewModel, EarningDto>();
            CreateMap<EarningDto, EarningViewModel>();

            CreateMap<ExpenseViewModel, ExpenseDto>();
            CreateMap<ExpenseDto, ExpenseViewModel>();

            CreateMap<UserGroupViewModel, UserGroupDto>();
            CreateMap<UserGroupDto, UserGroupViewModel>();
        }

        public override string ProfileName {
            get { return this.GetType().ToString(); }
        }
    }
}
