using BlazorFinances.Accessors;
using BlazorFinances.Accessors.Accessors;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Accessors.Models;
using BlazorFinances.Api.Config;
using BlazorFinances.Engines.Engines;
using BlazorFinances.Engines.Interfaces;
using BlazorFinances.Managers.Interfaces;
using BlazorFinances.Managers.Managers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Blazor.Server;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Text;

namespace BlazorFinances.Server
{
    public class Startup
    {
        private IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.AddMvc();

            services.AddTransient<IJwtTokenManager, JwtTokenManager>();
            services.AddTransient<ICategoryManager, CategoryManager>();
            services.AddTransient<IClaimResolverManager, ClaimResolverManager>();
            services.AddTransient<IExpenseManager, ExpenseManager>();
            services.AddTransient<IEarningManager, EarningManager>();
            services.AddTransient<IUserGroupManager, UserGroupManager>();
            services.AddTransient<IGraphManager, GraphManager>();
            services.AddTransient<IEmailManager, EmailManager>();
            services.AddTransient<IImportManager, ImportManager>();
            services.AddTransient<IGoalManager, GoalManager>();

            services.AddTransient<IEarningEngine, EarningEngine>();
            services.AddTransient<IExpenseEngine, ExpenseEngine>();
            services.AddTransient<IBarChartEngine, BarChartEngine>();
            services.AddTransient<IPieChartEngine, PieChartEngine>();
            services.AddTransient<ILineChartEngine, LineChartEngine>();
            services.AddTransient<IEmailEngine, EmailEngine>();
            services.AddTransient<IBankDataImportEngine, BankDataImportEngine>();

            services.AddTransient<ICategoryAccessor, CategoryAccessor>();
            services.AddTransient<IExpenseAccessor, ExpenseAccessor>();
            services.AddTransient<IEarningAccessor, EarningAccessor>();
            services.AddTransient<IUserGroupAccessor, UserGroupAccessor>();
            services.AddTransient<IGoalAccessor, GoalAccessor>();

            services.AddDefaultIdentity<ApplicationUser>().AddEntityFrameworkStores<FinancesContext>();

            services.AddResponseCompression(options =>
            {
                options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(new[]
                {
                    MediaTypeNames.Application.Octet,
                    WasmMediaTypeNames.Application.Wasm
                });
            });

            services.AddDbContext<FinancesContext>(options => options.UseSqlServer(_configuration.GetConnectionString("BlazorFinances.Accessors.FinancesContext")));
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = _configuration["Jwt:Issuer"],
                    ValidAudience = _configuration["Jwt:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]))
                };
            });

            //Register all automapper profiles
            AutoMapperRegistration.InitializeMapperProfiles();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");

            app.UseResponseCompression();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(name: "default", template: "{controller}/{action}/{id?}");
            });

            app.UseBlazor<Client.Startup>();

            //apply migrations manually at runtime. publish wizard currently doesn't support apply migrations on publish for ef core
            using (var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                scope.ServiceProvider.GetService<FinancesContext>().Database.Migrate();
            }
        }
    }
}
