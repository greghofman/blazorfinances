﻿using AutoMapper;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Models;
using BlazorFinances.Api.Controllers.Base;
using BlazorFinances.ClientApiShared.ViewModels;
using BlazorFinances.Managers.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BlazorFinances.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserGroupsController : BlazorControllerBase
    {
        private readonly IUserGroupManager _userGroupManager;

        public UserGroupsController(IUserGroupManager userGroupManager, IClaimResolverManager claimResolver) : base(claimResolver)
        {
            _userGroupManager = userGroupManager;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var userGroup = _userGroupManager.Get(_claimResolver.User.FindFirstValue(ClaimTypes.NameIdentifier));
                var userGroupViewModel = Mapper.Map<UserGroupViewModel>(userGroup);

                return Ok(userGroupViewModel ?? new UserGroupViewModel());
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(UserGroupsController), nameof(Get), ex));
                return BadRequest(ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] UserGroupViewModel vm)
        {
            try
            {
                var userGroupDto = Mapper.Map<UserGroupDto>(vm);
                var userGroupId = await _userGroupManager.Create(userGroupDto, _claimResolver.User.FindFirstValue(ClaimTypes.NameIdentifier));

                return Ok(userGroupId);
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(UserGroupsController), nameof(Create), ex));
                return BadRequest(ex);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Edit(UserGroupEditViewModel vm)
        {
            try
            {
                var groupEditDto = Mapper.Map<UserGroupEditDto>(vm);
                await _userGroupManager.Edit(_claimResolver.User.FindFirstValue(ClaimTypes.NameIdentifier), groupEditDto);

                return Ok(new { success = true });
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(UserGroupsController), nameof(Edit), ex));
                return BadRequest(ex);
            }
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(string email)
        {
            //todo: delete param, look user up by id and delete associated group
            //TODO: future introduce ceonept of group owner, only group owners can delete the entire group.
            //group members can only remove themselves
            try
            {
                await _userGroupManager.DeleteGroupMember(email);

                return Ok(new { deletedEmail = email });
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(UserGroupsController), nameof(Delete), ex));
                return BadRequest(ex);
            }
        }
    }
}