﻿using AutoMapper;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Api.Controllers.Base;
using BlazorFinances.ClientApiShared.ViewModels;
using BlazorFinances.Managers.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BlazorFinances.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ExpensesController : BlazorControllerBase
    {
        private readonly IExpenseManager _expenseManager;

        public ExpensesController(IExpenseManager expensesManager, IClaimResolverManager claimResolver) : base(claimResolver)
        {
            _expenseManager = expensesManager;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string sortOrder, string searchTerm, int? page = 0)
        {
            try
            {
                var expenseIndexDto = await _expenseManager.GetAll(_claimResolver.User.FindFirstValue(ClaimTypes.NameIdentifier), sortOrder, searchTerm, page.Value);
                //var expenseIndexViewModel = Mapper.Map<List<ExpensesIndexViewModel>>(expenseIndexDto);

                //todo: figure out automapper issue this is a temp workaround
                var expenseIndexViewModel = new ExpensesIndexViewModel
                {
                    Expenses = Mapper.Map<List<ExpenseViewModel>>(expenseIndexDto.Expenses),
                    SortOrder = expenseIndexDto.SortOrder,
                    SearchTerm = expenseIndexDto.SearchTerm,
                    Page = expenseIndexDto.Page,
                    TotalPages = expenseIndexDto.TotalPages,
                    TotalRecords = expenseIndexDto.TotalRecords,
                    TotalFilteredRecords = expenseIndexDto.TotalFilteredRecords
                };

                return Ok(expenseIndexViewModel);
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(ExpensesController), nameof(Get), ex));
                return BadRequest(ex);
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var expense = _expenseManager.Get(id);
                var expenseViewModel = Mapper.Map<ExpenseViewModel>(expense);

                return Ok(expenseViewModel);
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(ExpensesController), nameof(Get), ex));
                return BadRequest(ex);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] ExpenseViewModel vm)
        {
            try
            {
                vm.UserId = _claimResolver.User.FindFirstValue(ClaimTypes.NameIdentifier);
                var expense = Mapper.Map<ExpenseDto>(vm);
                var expenseId = _expenseManager.Create(expense);

                return Ok(expenseId);
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(ExpensesController), nameof(Create), ex));
                return BadRequest(ex);
            }
        }

        [HttpPut]
        public IActionResult Edit([FromBody] ExpenseViewModel vm)
        {
            try
            {
                var expenseDto = Mapper.Map<ExpenseDto>(vm);
                var editedId = _expenseManager.Edit(expenseDto);

                return Ok(new { id = editedId });
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(ExpensesController), nameof(Edit), ex));
                return BadRequest(ex);
            }
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            try
            {
                var deletedId = _expenseManager.Delete(id);

                return Ok(new { id = deletedId });
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(ExpensesController), nameof(Delete), ex));
                return BadRequest(ex);
            }
        }
    }
}