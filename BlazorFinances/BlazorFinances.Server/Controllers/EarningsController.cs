﻿using AutoMapper;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.ClientApiShared.ViewModels;
using BlazorFinances.Managers.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using BlazorFinances.Api.Controllers.Base;
using System.Linq;

namespace BlazorFinances.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EarningsController : BlazorControllerBase
    {
        private readonly IEarningManager _earningManager;

        public EarningsController(IEarningManager earningManager, IClaimResolverManager claimResolver) : base(claimResolver)
        {
            _earningManager = earningManager;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string sortOrder, string searchTerm, int? page = 0)
        {
            try
            {
                var earningsIndexDto = await _earningManager.GetAll(_claimResolver.User.FindFirstValue(ClaimTypes.NameIdentifier), sortOrder, searchTerm, page.Value);
                var earningViewModel = Mapper.Map<EarningsIndexViewModel>(earningsIndexDto);

                return Ok(earningViewModel);
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(EarningsController), nameof(Get), ex));
                return BadRequest(ex);
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var earning = _earningManager.Get(id);
                var earningViewModel = Mapper.Map<EarningViewModel>(earning);

                return Ok(earningViewModel);
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(EarningsController), nameof(Get), ex));
                return BadRequest(ex);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] EarningViewModel vm)
        {
            try
            {
                vm.UserId = _claimResolver.User.FindFirstValue(ClaimTypes.NameIdentifier);
                var earning = Mapper.Map<EarningDto>(vm);
                var earningId = _earningManager.Create(earning);

                return Ok(earningId);
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(EarningsController), nameof(Create), ex));
                return BadRequest(ex);
            }
        }

        [HttpPut]
        public IActionResult Edit([FromBody] EarningViewModel vm)
        {
            try
            {
                var earningDto = Mapper.Map<EarningDto>(vm);
                var editedId = _earningManager.Edit(earningDto);

                return Ok(new { id = editedId });
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(EarningsController), nameof(Edit), ex));
                return BadRequest(ex);
            }
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            try
            {
                var deletedId = _earningManager.Delete(id);

                return Ok(new { id = deletedId });
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(EarningsController), nameof(Delete), ex));
                return BadRequest(ex);
            }
        }
    }
}