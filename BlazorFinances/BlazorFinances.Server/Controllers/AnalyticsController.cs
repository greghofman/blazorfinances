﻿using AutoMapper;
using BlazorFinances.Api.Controllers.Base;
using BlazorFinances.ClientApiShared.ViewModels;
using BlazorFinances.Managers.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BlazorFinances.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AnalyticsController : BlazorControllerBase
    {
        private IGraphManager _graphManager;

        public AnalyticsController(IGraphManager graphManager, IClaimResolverManager claimResolver) : base(claimResolver)
        {
            _graphManager = graphManager;
        }

        [HttpGet]
        [Route("ExpensesEarningsBarChart")]
        public async Task<IActionResult> GetExpensesEarningsBarChart()
        {
            try
            {
                var userId = _claimResolver.User.FindFirstValue(ClaimTypes.NameIdentifier);
                var barChartDto = await _graphManager.BuildExpensesEarningsBarChart(userId);
                var barChartVm = Mapper.Map<BarChartViewModel>(barChartDto);
                
                return Ok(barChartVm);
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(AnalyticsController), nameof(GetExpensesEarningsBarChart), ex));
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("ExpensesPieChart")]
        public async Task<IActionResult> GetExpensesPieChart()
        {
            try
            {
                var userId = _claimResolver.User.FindFirstValue(ClaimTypes.NameIdentifier);
                var pieChartDto = await _graphManager.BuildExpensesPieChart(userId);
                var pieChartVm = Mapper.Map<PieChartViewModel>(pieChartDto);

                return Ok(pieChartVm);
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(AnalyticsController), nameof(GetExpensesPieChart), ex));
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("ExpenseGoalLineChart")]
        public async Task<IActionResult> GetExpenseGoalLineChart(int? goalId)
        {
            try
            {
                var userId = _claimResolver.User.FindFirstValue(ClaimTypes.NameIdentifier);
                var lineChartDto = await _graphManager.BuildExpenseGoalLineChart(userId, goalId);
                var lineChartVm = Mapper.Map<GoalLineChartViewModel>(lineChartDto);

                return Ok(lineChartVm);
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(AnalyticsController), nameof(GetExpenseGoalLineChart), ex));
                return BadRequest(ex);
            }
        }
    }
}