﻿using AutoMapper;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Api.Controllers.Base;
using BlazorFinances.ClientApiShared.ViewModels;
using BlazorFinances.Managers.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BlazorFinances.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class GoalsController : BlazorControllerBase
    {
        private readonly IGoalManager _goalManager;

        public GoalsController(IGoalManager goalManager, IClaimResolverManager claimResolver) : base(claimResolver)
        {
            _goalManager = goalManager;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var goalIndexDtos = _goalManager.GetAll(_claimResolver.User.FindFirstValue(ClaimTypes.NameIdentifier));
                var goalViewModels = Mapper.Map<List<GoalViewModel>>(goalIndexDtos);

                return Ok(goalViewModels);
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(GoalsController), nameof(Get), ex));
                return BadRequest(ex);
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var goal = _goalManager.Get(id);
                var goalViewModel = Mapper.Map<GoalViewModel>(goal);

                return Ok(goalViewModel);
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(GoalsController), nameof(Get), ex));
                return BadRequest(ex);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] GoalViewModel vm)
        {
            try
            {
                vm.UserId = _claimResolver.User.FindFirstValue(ClaimTypes.NameIdentifier);
                var goal = Mapper.Map<GoalDto>(vm);
                var goalId = _goalManager.Create(goal);

                return Ok(goalId);
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(GoalsController), nameof(Create), ex));
                return BadRequest(ex);
            }
        }

        [HttpPut]
        public IActionResult Edit([FromBody] GoalViewModel vm)
        {
            try
            {
                var goalDto = Mapper.Map<GoalDto>(vm);
                var editedId = _goalManager.Edit(goalDto);

                return Ok(new { id = editedId });
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(GoalsController), nameof(Edit), ex));
                return BadRequest(ex);
            }
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            try
            {
                var deletedId = _goalManager.Delete(id);

                return Ok(new { id = deletedId });
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(GoalsController), nameof(Delete), ex));
                return BadRequest(ex);
            }
        }
    }
}
