﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Api.Controllers.Base;
using BlazorFinances.ClientApiShared.ViewModels;
using BlazorFinances.Managers.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BlazorFinances.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ImportController : BlazorControllerBase
    {
        private IImportManager _importManager;

        public ImportController(IImportManager importManager, IClaimResolverManager claimResolver) : base(claimResolver)
        {
            _importManager = importManager;
        }

        [HttpPost]
        public IActionResult ImportCsvBankTransactions(TransactionImportViewModel importViewModel)
        {
            try
            {
                importViewModel.UserId = _claimResolver.User.FindFirstValue(ClaimTypes.NameIdentifier);
                var result = _importManager.ImportCsvBankTransactions(Mapper.Map<TransactionImportDto>(importViewModel));

                return Ok(new { success = result });
            }
            catch(Exception ex)
            {
                log.Error(FormatError(nameof(ImportController), nameof(ImportCsvBankTransactions), ex));
                return BadRequest(ex);
            }
        }
    }
}