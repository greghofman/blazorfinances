﻿using BlazorFinances.Managers.Interfaces;
using log4net;
using Microsoft.AspNetCore.Mvc;
using System;

namespace BlazorFinances.Api.Controllers.Base
{
    public abstract class BlazorControllerBase : ControllerBase
    {
        protected IClaimResolverManager _claimResolver;

        public BlazorControllerBase(IClaimResolverManager claimResolverManager)
        {
            _claimResolver = claimResolverManager;
        }

        protected static readonly ILog log = LogManager.GetLogger(typeof(BlazorControllerBase));

        protected string FormatError(string controllerName, string actionName, Exception ex)
        {
            var message = $"{controllerName} : {actionName} ::: {ex.Message}";
            if (ex.InnerException != null)
            {
                message = message + $" ::: {ex.InnerException.Message}";
            }

            return message;
        }
    }
}
