﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Models;
using BlazorFinances.Api.Controllers.Base;
using BlazorFinances.ClientApiShared.ViewModels.Account;
using BlazorFinances.Common.Types;
using BlazorFinances.Managers.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace BlazorFinances.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : BlazorControllerBase
    {
        private readonly IJwtTokenManager _tokenManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailManager _emailManager;
        private IConfiguration _config;

        //todo: appuser dto and appuser vm
        public AuthController(IJwtTokenManager tokenManager,
            UserManager<ApplicationUser> userManager,
            IEmailManager emailManager,
            IClaimResolverManager claimResolver,
            IConfiguration config) : base(claimResolver)
        {
            _tokenManager = tokenManager;
            _userManager = userManager;
            _emailManager = emailManager;
            _config = config;
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register([FromBody] TokenViewModel vm)
        {
            try
            {
                var user = new ApplicationUser()
                {
                    UserName = vm.Email,
                    Email = vm.Email
                };

                var result = await _userManager.CreateAsync(user, vm.Password);

                if (!result.Succeeded)
                {
                    return StatusCode(500);
                }

                var confirmEmailToken = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                var callbackUrl = _config["HostRootUrl"] + "/confirmemail?token=" + confirmEmailToken + "&userId=" + user.Id;

                var applicationUserDto = Mapper.Map<ApplicationUserDto>(user);
                var emailSuccess = await _emailManager.SendAsync(EmailType.ConfirmEmail, callbackUrl, applicationUserDto);

                return Ok(new { success = emailSuccess });
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(AuthController), nameof(Register), ex));
                return BadRequest(ex);
            }

        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] TokenViewModel vm)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(vm.Email);
                var isLoginRequestValid = await _userManager.CheckPasswordAsync(user, vm.Password);

                if (!isLoginRequestValid)
                {
                    return BadRequest(new TokenViewModel { IsSuccessful = false });
                }

                if (!user.EmailConfirmed)
                {
                    return BadRequest(new TokenViewModel { IsSuccessful = true, IsEmailConfirmed = false });
                }

                return Ok(new TokenViewModel { Token = _tokenManager.BuildJwtToken(vm.Email, user.Id), IsSuccessful = true, IsEmailConfirmed = true });
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(AuthController), nameof(Login), ex));
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [Route("ConfirmEmail")]
        public async Task<IActionResult> ConfirmEmail(ConfirmEmailViewModel vm)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(vm.UserId);
                vm.Token = vm.Token.Replace(' ', '+');
                var result = await _userManager.ConfirmEmailAsync(user, vm.Token);

                if (result.Succeeded)
                {
                    return Ok("success");
                }
                else
                {
                    return BadRequest("Unable to confirm email");
                }
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(AuthController), nameof(ConfirmEmail), ex));
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("ResetPassword")]
        public async Task<IActionResult> ResetPassword(PasswordResetViewModel vm)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(vm.UserId);
                vm.Token = vm.Token.Replace(' ', '+');
                var result = await _userManager.ResetPasswordAsync(user, vm.Token, vm.Password);

                if (result.Succeeded)
                {
                    return Ok(true);
                }
                else
                {
                    return BadRequest("Unable to reset password");
                }
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(AuthController), nameof(ConfirmEmail), ex));
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("ForgotPassword")]
        public async Task<IActionResult> ForgotPassword(string email)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(email);
                var passwordResetToken = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = _config["HostRootUrl"] + "/resetpassword?token=" + passwordResetToken + "&userId=" + user.Id;

                await _emailManager.SendAsync(EmailType.ForgotPassword, callbackUrl, Mapper.Map<ApplicationUserDto>(user));

                return Ok(true);
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(AuthController), nameof(ForgotPassword), ex));
                return BadRequest(ex);
            }
        }
    }
}