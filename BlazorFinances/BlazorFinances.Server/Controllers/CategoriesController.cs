﻿using System;
using System.Collections.Generic;
using AutoMapper;
using BlazorFinances.Accessors.DTOs;
using BlazorFinances.ClientApiShared.ViewModels;
using Microsoft.AspNetCore.Mvc;
using BlazorFinances.Managers.Interfaces;
using Microsoft.AspNetCore.Authorization;
using BlazorFinances.Api.Controllers.Base;

namespace BlazorFinances.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CategoriesController : BlazorControllerBase
    {
        private readonly ICategoryManager _categoryManager;

        public CategoriesController(ICategoryManager categoryManager, IClaimResolverManager claimResolver) : base(claimResolver)
        {
            _categoryManager = categoryManager;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var categories = _categoryManager.GetAll();
                var categoryViewModels = Mapper.Map<List<CategoryViewModel>>(categories);

                return Ok(categoryViewModels);
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(CategoriesController), nameof(Get), ex));
                return BadRequest(ex);
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var category = _categoryManager.Get(id);
                var categoryViewModel = Mapper.Map<CategoryViewModel>(category);

                return Ok(categoryViewModel);
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(CategoriesController), nameof(Get), ex));
                return BadRequest(ex);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] CategoryViewModel vm) 
        {
            try
            {
                var categoryDto = Mapper.Map<CategoryDto>(vm);
                var categoryId = _categoryManager.Create(categoryDto);

                return Ok(categoryId);
            }
            catch(Exception ex)
            {
                log.Error(FormatError(nameof(CategoriesController), nameof(Create), ex));
                return BadRequest(ex);
            }
        }

        [HttpPut]
        public IActionResult Edit([FromBody] CategoryViewModel vm)
        {
            try
            {
                var categoryDto = Mapper.Map<CategoryDto>(vm);
                var editedId = _categoryManager.Edit(categoryDto);

                return Ok(new { id = editedId });
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(CategoriesController), nameof(Edit), ex));
                return BadRequest(ex);
            }
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            try
            {
                var deletedId = _categoryManager.Delete(id);

                return Ok(new { id = deletedId });
            }
            catch (Exception ex)
            {
                log.Error(FormatError(nameof(CategoriesController), nameof(Delete), ex));
                return BadRequest(ex);
            }
        }
    }
}