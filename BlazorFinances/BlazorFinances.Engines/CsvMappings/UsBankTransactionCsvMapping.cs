﻿using BlazorFinances.Accessors.DTOs;
using TinyCsvParser.Mapping;

namespace BlazorFinances.Engines.CsvMappings
{
    public class UsBankTransactionCsvMapping : CsvMapping<TransactionDto>
    {
        public UsBankTransactionCsvMapping()
        : base()
        {
            MapProperty(0, x => x.OccurenceDate);
            MapProperty(2, x => x.Name);
            MapProperty(4, x => x.Amount);
        }
    }
}
