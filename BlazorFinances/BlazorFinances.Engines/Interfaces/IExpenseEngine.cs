﻿using BlazorFinances.Accessors.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorFinances.Engines.Interfaces
{
    public interface IExpenseEngine
    {
        List<ExpenseDto> GetExpensesForGroup(ApplicationUserDto user);

        ExpensesIndexDto GetExpensesForIndex(ApplicationUserDto user, string sortOrder, string searchTerm, int page);
    }
}
