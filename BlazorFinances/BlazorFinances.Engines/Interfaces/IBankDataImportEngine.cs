﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BlazorFinances.Engines.Interfaces
{
    public interface IBankDataImportEngine
    {
        Task<string> GetFileContents(IFormFile file);

        bool ImportUsBankTransactionCsvData(string fileContents, string userId);
    }
}
