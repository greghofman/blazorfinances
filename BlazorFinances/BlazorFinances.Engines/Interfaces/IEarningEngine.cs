﻿using BlazorFinances.Accessors.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BlazorFinances.Engines.Interfaces
{
    public interface IEarningEngine
    {
        List<EarningDto> GetEarningsForGroup(ApplicationUserDto user);

        EarningsIndexDto GetEarningsForIndex(ApplicationUserDto user, string sortOrder, string searchTerm, int page);
    }
}
