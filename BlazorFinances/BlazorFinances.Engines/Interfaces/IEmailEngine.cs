﻿using BlazorFinances.Accessors.DTOs;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BlazorFinances.Engines.Interfaces
{
    public interface IEmailEngine
    {
        MailMessage GetConfirmEmail(string callbackUrl, ApplicationUserDto user);

        MailMessage GetForgotPasswordEmail(string callbackUrl, ApplicationUserDto user);

        Task<bool> SendEmail(MailMessage email);
    }
}
