﻿using BlazorFinances.Accessors.DTOs;
using System.Collections.Generic;

namespace BlazorFinances.Engines.Interfaces
{
    public interface ILineChartEngine
    {
        GoalLineChartDto BuildExpenseGoalLineChart(List<ExpenseDto> groupExpenses, int? goalId, string userId);
    }
}
