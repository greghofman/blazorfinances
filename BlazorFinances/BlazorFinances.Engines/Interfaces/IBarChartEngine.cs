﻿using BlazorFinances.Accessors.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorFinances.Engines.Interfaces
{
    public interface IBarChartEngine
    {
        BarChartDto BuildExpenseEarningBarChart(List<ExpenseDto> expenses, List<EarningDto> earnings);
    }
}
