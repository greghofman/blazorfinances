﻿using BlazorFinances.Accessors.DTOs;
using System.Collections.Generic;

namespace BlazorFinances.Engines.Interfaces
{
    public interface IPieChartEngine
    {
        PieChartDto BuildExpensesPieChart(List<ExpenseDto> expenses);
    }
}
