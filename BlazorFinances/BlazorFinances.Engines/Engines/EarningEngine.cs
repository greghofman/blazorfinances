﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Engines.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlazorFinances.Engines.Engines
{
    public class EarningEngine : IEarningEngine
    {
        private IEarningAccessor _earningAccessor;
        private IUserGroupAccessor _userGroupAccessor;

        public EarningEngine(IEarningAccessor earningAccessor, IUserGroupAccessor userGroupAccessor)
        {
            _earningAccessor = earningAccessor;
            _userGroupAccessor = userGroupAccessor;
        }

        public List<EarningDto> GetEarningsForGroup(ApplicationUserDto user)
        {
            var earningDtos = new List<EarningDto>();

            if (user.GroupId.HasValue)
            {
                var group = _userGroupAccessor.Get(user.GroupId.Value);
                var groupUserIds = group.Users.Select(x => x.Id).ToList();
                earningDtos = _earningAccessor.GetAll(groupUserIds);
            }
            else
            {
                earningDtos = _earningAccessor.GetAll(user.Id);
            }

            return earningDtos;
        }

        public EarningsIndexDto GetEarningsForIndex(ApplicationUserDto user, string sortOrder, string searchTerm, int page)
        {
            var earningsIndexDto = new EarningsIndexDto();

            if (user.GroupId.HasValue)
            {
                var group = _userGroupAccessor.Get(user.GroupId.Value);
                var groupUserIds = group.Users.Select(x => x.Id).ToList();
                earningsIndexDto = _earningAccessor.GetAllForIndex(groupUserIds, sortOrder, searchTerm, page);
            }
            else
            {
                earningsIndexDto = _earningAccessor.GetAllForIndex(user.Id, sortOrder, searchTerm, page);
            }

            return earningsIndexDto;
        }
    }
}
