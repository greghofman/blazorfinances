﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Engines.Interfaces;
using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;

namespace BlazorFinances.Engines.Engines
{
    public class EmailEngine : IEmailEngine
    {
        public MailMessage GetConfirmEmail(string callbackUrl, ApplicationUserDto user)
        {
#if DEBUG
            var body = File.ReadAllText(@"..\BlazorFinances.Client\Pages\EmailTemplates\EmailConfirm.html");
            var image = new Attachment(@"..\BlazorFinances.Client\wwwroot\images\finance-hero.jpg");
#else
            var body = File.ReadAllText(@"/home/site/wwwroot/Pages/EmailTemplates/EmailConfirm.html");
            var image = new Attachment(@"/home/site/wwwroot/BlazorFinances.Client/dist/images/finance-hero.jpg");
#endif

            body = body.Replace("{{email}}", user.Email);
            body = body.Replace("{{callbackUrl}}", callbackUrl);
            var x = AppDomain.CurrentDomain.BaseDirectory;

            var contentId = Guid.NewGuid().ToString();
            image.ContentId = contentId;
            image.ContentDisposition.Inline = true;
            image.ContentDisposition.DispositionType = DispositionTypeNames.Inline;

            body = body.Replace("{{imageContentId}}", "cid:" + contentId);

            var email = new MailMessage
            {
                Subject = "Confirm Your Email",
                Body = body,
                IsBodyHtml = true,
                From = new MailAddress("blazor.finances@gmail.com", "Blazor Finances"),
            };

            email.Attachments.Add(image);
            email.To.Insert(0, new MailAddress(user.Email));

            return email;
        }

        public MailMessage GetForgotPasswordEmail(string callbackUrl, ApplicationUserDto user)
        {
#if DEBUG
            var body = File.ReadAllText(@"..\BlazorFinances.Client\Pages\EmailTemplates\ForgotPassword.html");
            var image = new Attachment(@"..\BlazorFinances.Client\wwwroot\images\finance-hero.jpg");
#else
            var body = File.ReadAllText(@"/home/site/wwwroot/Pages/EmailTemplates/ForgotPassword.html");
            var image = new Attachment(@"/home/site/wwwroot/BlazorFinances.Client/dist/images/finance-hero.jpg");
#endif

            body = body.Replace("{{email}}", user.Email);
            body = body.Replace("{{callbackUrl}}", callbackUrl);

            var contentId = Guid.NewGuid().ToString();
            image.ContentId = contentId;
            image.ContentDisposition.Inline = true;
            image.ContentDisposition.DispositionType = DispositionTypeNames.Inline;

            body = body.Replace("{{imageContentId}}", "cid:" + contentId);

            var email = new MailMessage
            {
                Subject = "Reset Your Password",
                Body = body,
                IsBodyHtml = true,
                From = new MailAddress("blazor.finances@gmail.com", "Blazor Finances"),
            };

            email.Attachments.Add(image);
            email.To.Insert(0, new MailAddress(user.Email));

            return email;
        }

        public async Task<bool> SendEmail(MailMessage email)
        {
            //TODO: read from config
            using (var client = new SmtpClient("smtp.gmail.com", 587))
            {
                client.UseDefaultCredentials = false;
                //TODO: read correct password from app config
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Credentials = new NetworkCredential("blazor.finances@gmail.com", "ChangeMePlease1!");
                await client.SendMailAsync(email);
            }

            return true;
        }
    }
}
