﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Engines.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlazorFinances.Engines.Engines
{
    public class ExpenseEngine : IExpenseEngine
    {
        private IUserGroupAccessor _userGroupAccessor;
        private IExpenseAccessor _expenseAccessor;

        public ExpenseEngine(IUserGroupAccessor userGroupAccessor, IExpenseAccessor expenseAccessor)
        {
            _userGroupAccessor = userGroupAccessor;
            _expenseAccessor = expenseAccessor;
        }

        public List<ExpenseDto> GetExpensesForGroup(ApplicationUserDto user)
        {
            var expenseDtos = new List<ExpenseDto>();

            if (user.GroupId.HasValue)
            {
                var group = _userGroupAccessor.Get(user.GroupId.Value);
                var groupUserIds = group.Users.Select(x => x.Id).ToList();
                expenseDtos = _expenseAccessor.GetAll(groupUserIds);
            }
            else
            {
                expenseDtos = _expenseAccessor.GetAll(user.Id);
            }

            return expenseDtos;
        }

        public ExpensesIndexDto GetExpensesForIndex(ApplicationUserDto user, string sortOrder, string searchTerm, int page)
        {
            var expensesIndexDto = new ExpensesIndexDto();

            if (user.GroupId.HasValue)
            {
                var group = _userGroupAccessor.Get(user.GroupId.Value);
                var groupUserIds = group.Users.Select(x => x.Id).ToList();
                expensesIndexDto = _expenseAccessor.GetAllForIndex(groupUserIds, sortOrder, searchTerm, page);
            }
            else
            {
                expensesIndexDto = _expenseAccessor.GetAllForIndex(user.Id, sortOrder, searchTerm, page);
            }

            return expensesIndexDto;
        }
    }
}
