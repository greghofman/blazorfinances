﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Engines.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlazorFinances.Engines.Engines
{
    public class PieChartEngine : IPieChartEngine
    {
        private readonly List<string> _backgroundColors = new List<string> { "#F8A51B", "#70BE44", "#00ACAC", "#21409A", "#A3218E", "#BB302D", "#FEF102", "#03A45E", "#0465B2", "#592F93", "#EE1C25" };

        public PieChartDto BuildExpensesPieChart(List<ExpenseDto> expenses)
        {
            var sixMonthsAgo = DateTime.Now.AddMonths(-6);
            var pieChartItems = expenses.Where(x => x.OccurenceDate < DateTime.Now && x.OccurenceDate > sixMonthsAgo).GroupBy(x => x.CategoryId)
                .Select(y => new PieChartItemDto
                {
                    Amount = y.Sum(z => z.Amount),
                    Label = (y.First().CategoryId.HasValue ? y.First().Category.Name : "Uncategorized")
                })
                .ToList();

            return new PieChartDto { PieChartItems = pieChartItems, BackgroundColors = _backgroundColors };
        }
    }
}
