﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Engines.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlazorFinances.Engines.Engines
{
    public class LineChartEngine : ILineChartEngine
    {
        private IGoalAccessor _goalAccessor;

        public LineChartEngine(IGoalAccessor goalAccessor)
        {
            _goalAccessor = goalAccessor;
        }

        public GoalLineChartDto BuildExpenseGoalLineChart(List<ExpenseDto> groupExpenses, int? goalId, string userId)
        {
            DateTime mostRecentExpense;
            DateTime mostRecentEarning;
            GoalDto goalDto = null;

            if (goalId.HasValue)
            {
                goalDto = _goalAccessor.Get(goalId.Value);
            }
            else
            {
                goalDto = _goalAccessor.GetAll(userId).FirstOrDefault();
                if(goalDto == null)
                {
                    return null;
                }
            }

            var expensesInGoalCategory = groupExpenses;

            if (!goalDto.IsForTotalExpenses)
            {
                expensesInGoalCategory = groupExpenses.Where(x => x.CategoryId == goalDto.CategoryId).ToList();
            }
            
            //build list of last 6 month names for month labels on x axis
            if (expensesInGoalCategory != null && expensesInGoalCategory.Count > 0)
            {
                mostRecentExpense = expensesInGoalCategory.Select(x => x.OccurenceDate).Max();
            }
            else
            {
                mostRecentExpense = DateTime.Now;
            }

            if (expensesInGoalCategory != null && expensesInGoalCategory.Count > 0)
            {
                mostRecentEarning = expensesInGoalCategory.Select(x => x.OccurenceDate).Max();
            }
            else
            {
                mostRecentEarning = DateTime.Now;
            }

            var mostRecentEntry = mostRecentExpense >= mostRecentEarning ? mostRecentExpense : mostRecentEarning;

            var prevSixMonthNames = new List<string>
            {
                mostRecentEntry.AddMonths(-5).ToString("MMMM"),
                mostRecentEntry.AddMonths(-4).ToString("MMMM"),
                mostRecentEntry.AddMonths(-3).ToString("MMMM"),
                mostRecentEntry.AddMonths(-2).ToString("MMMM"),
                mostRecentEntry.AddMonths(-1).ToString("MMMM"),
                mostRecentEntry.ToString("MMMM")
            };

            var prevSixMonthExpenses = new List<object>
            {
                expensesInGoalCategory.Where(x => x.OccurenceDate.Month == mostRecentEntry.AddMonths(-5).Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum(),
                expensesInGoalCategory.Where(x => x.OccurenceDate.Month == mostRecentEntry.AddMonths(-4).Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum(),
                expensesInGoalCategory.Where(x => x.OccurenceDate.Month == mostRecentEntry.AddMonths(-3).Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum(),
                expensesInGoalCategory.Where(x => x.OccurenceDate.Month == mostRecentEntry.AddMonths(-2).Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum(),
                expensesInGoalCategory.Where(x => x.OccurenceDate.Month == mostRecentEntry.AddMonths(-1).Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum(),
                expensesInGoalCategory.Where(x => x.OccurenceDate.Month == mostRecentEntry.Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum()
            };

            var goalAmounts = new List<object>();
            foreach(var amount in prevSixMonthExpenses)
            {
                goalAmounts.Add(goalDto.Amount);
            }

            return new GoalLineChartDto
            {
                MonthlyAmounts = prevSixMonthExpenses,
                MonthLabels = prevSixMonthNames,
                GoalAmounts = goalAmounts
            };

        }
    }
}
