﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Engines.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlazorFinances.Engines.Engines
{
    public class BarChartEngine : IBarChartEngine
    {
        public BarChartDto BuildExpenseEarningBarChart(List<ExpenseDto> expenses, List<EarningDto> earnings)
        {
            DateTime mostRecentExpense;
            DateTime mostRecentEarning;

            //build list of last 6 month names for month labels on x axis
            if(expenses != null && expenses.Count > 0)
            {
                mostRecentExpense = expenses.Select(x => x.OccurenceDate).Max();
            }
            else
            {
                mostRecentExpense = DateTime.Now;
            }

            if(earnings != null && earnings.Count> 0)
            {
                mostRecentEarning = expenses.Select(x => x.OccurenceDate).Max();
            }
            else
            {
                mostRecentEarning = DateTime.Now;
            }
            
            var mostRecentEntry = mostRecentExpense >= mostRecentEarning ? mostRecentExpense : mostRecentEarning;

            var prevSixMonthNames = new List<string>
            {
                mostRecentEntry.AddMonths(-5).ToString("MMMM"),
                mostRecentEntry.AddMonths(-4).ToString("MMMM"),
                mostRecentEntry.AddMonths(-3).ToString("MMMM"),
                mostRecentEntry.AddMonths(-2).ToString("MMMM"),
                mostRecentEntry.AddMonths(-1).ToString("MMMM"),
                mostRecentEntry.ToString("MMMM")
            };

            var prevSixMonthExpenses = new List<object>
            {
                expenses.Where(x => x.OccurenceDate.Month == mostRecentEntry.AddMonths(-5).Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum(),
                expenses.Where(x => x.OccurenceDate.Month == mostRecentEntry.AddMonths(-4).Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum(),
                expenses.Where(x => x.OccurenceDate.Month == mostRecentEntry.AddMonths(-3).Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum(),
                expenses.Where(x => x.OccurenceDate.Month == mostRecentEntry.AddMonths(-2).Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum(),
                expenses.Where(x => x.OccurenceDate.Month == mostRecentEntry.AddMonths(-1).Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum(),
                expenses.Where(x => x.OccurenceDate.Month == mostRecentEntry.Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum()
            };

            var prevSixMonthEarnings = new List<object>
            {
                earnings.Where(x => x.OccurenceDate.Month == mostRecentEntry.AddMonths(-5).Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum(),
                earnings.Where(x => x.OccurenceDate.Month == mostRecentEntry.AddMonths(-4).Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum(),
                earnings.Where(x => x.OccurenceDate.Month == mostRecentEntry.AddMonths(-3).Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum(),
                earnings.Where(x => x.OccurenceDate.Month == mostRecentEntry.AddMonths(-2).Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum(),
                earnings.Where(x => x.OccurenceDate.Month == mostRecentEntry.AddMonths(-1).Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum(),
                earnings.Where(x => x.OccurenceDate.Month == mostRecentEntry.Month && x.OccurenceDate.Year == mostRecentEntry.Year).Select(x => x.Amount).Sum()
            };

            var barChartDto = new BarChartDto
            {
                EarningData = prevSixMonthEarnings,
                ExpenseData = prevSixMonthExpenses,
                MonthsLabels = prevSixMonthNames
            };

            return barChartDto;
        }
    }
}
