﻿using BlazorFinances.Accessors.DTOs;
using BlazorFinances.Accessors.Interfaces;
using BlazorFinances.Engines.CsvMappings;
using BlazorFinances.Engines.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TinyCsvParser;

namespace BlazorFinances.Engines.Engines
{
    public class BankDataImportEngine : IBankDataImportEngine
    {
        private IExpenseAccessor _expenseAccessor;
        private IEarningAccessor _earningAccessor;

        public BankDataImportEngine(IEarningAccessor earningAccessor, IExpenseAccessor expenseAccessor)
        {
            _earningAccessor = earningAccessor;
            _expenseAccessor = expenseAccessor;
        }

        public async Task<string> GetFileContents(IFormFile file)
        {
            var fileContents = string.Empty;
            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                fileContents = await reader.ReadToEndAsync();
            }

            return fileContents;
        }

        public bool ImportUsBankTransactionCsvData(string fileContents, string userId)
        {
            CsvParserOptions csvParserOptions = new CsvParserOptions(true, ',');
            CsvReaderOptions csvReaderOptions = new CsvReaderOptions(new[] { "\n" });
            UsBankTransactionCsvMapping csvMapper = new UsBankTransactionCsvMapping();
            CsvParser<TransactionDto> csvParser = new CsvParser<TransactionDto>(csvParserOptions, csvMapper);

            var result = csvParser
                .ReadFromString(csvReaderOptions, fileContents)
                .ToList();

            var transactionDtos = result.Select(x => x.Result).ToList();

            var earningDtos = transactionDtos
                .Where(x => x.Amount > 0)
                .Select(x => new EarningDto { Name = x.Name, Amount = x.Amount, OccurenceDate = x.OccurenceDate })
                .ToList();
            earningDtos.ForEach(x => x.UserId = userId);

            var expenseDtos = transactionDtos
                .Where(x => x.Amount <= 0)
                .Select(x => new ExpenseDto { Name = x.Name, Amount = Math.Abs(x.Amount), OccurenceDate = x.OccurenceDate })
                .ToList();
            expenseDtos.ForEach(x => x.UserId = userId);

            _earningAccessor.CreateAll(earningDtos);
            _expenseAccessor.CreateAll(expenseDtos);

            return true;
        }
    }
}
