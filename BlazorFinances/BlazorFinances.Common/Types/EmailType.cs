﻿namespace BlazorFinances.Common.Types
{
    public enum EmailType
    {
        ConfirmEmail = 0,

        ForgotPassword = 1
    }
}
